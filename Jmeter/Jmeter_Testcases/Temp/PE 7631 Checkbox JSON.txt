{
  "entities": [
    {
      "data": [
        {          
          "STI_049": {
            "STI_Emp_GRP_01": 
              "1"
            ,
            "STI_Emp_GRP_02": 
              "2"
            ,
            "STI_Emp_GRP_03": 
              "2"
            ,
            "STI_Emp_GRP_04": 
              "3"
            ,
            "STI_Emp_GRP_05": 
              "4"
            ,
            "STI_Emp_GRP_06": 
              "5"
            ,
            "STI_Emp_GRP_07": 
              "6"
            ,
            "STI_Emp_GRP_08": 
              "7"
            ,
            "STI_Emp_GRP_09": 
              "8"
            ,
            "STI_Emp_GRP_10": 
              "9"
            ,
            "STI_Emp_GRP_11": 
              "10"
            ,
            "STI_Emp_GRP_12": 
              "11"
            
          }
        }
      ],
      "sectionStructure": {
        "columns": [          
          {
            "code": "STI_049",
            "dataType": "string",
            "questionType": "checkboxes",
            "validations": [],
            "options": {
              "items": [
                {
                  "value": "1",
                  "displayName": "Organization Performance"
                },
                {
                  "value": "2",
                  "displayName": "Business Unit/Division Performance"
                },
                {
                  "value": "3",
                  "displayName": "Team Performance (Department's performance)"
                },
                {
                  "value": "4",
                  "displayName": "Individual Performance"
                },
                {
                  "value": "5",
                  "displayName": "Individual Financial Performance"
                },
                {
                  "value": "6",
                  "displayName": "Individual Non-Financial Performance"
                },
                {
                  "value": "7",
                  "displayName": "Global"
                },
                {
                  "value": "8",
                  "displayName": "Regional Performance (e.g. Europe)"
                },
                {
                  "value": "9",
                  "displayName": "Country"
                },
                {
                  "value": "10",
                  "displayName": "Other"
                },
                {
                  "value": "11",
                  "displayName": "No fix criteria"
                }
              ]
            },
            "dimensions": [
              "STI_Emp_GRP_01",
              "STI_Emp_GRP_02",
              "STI_Emp_GRP_03",
              "STI_Emp_GRP_04",
              "STI_Emp_GRP_05",
              "STI_Emp_GRP_06",
              "STI_Emp_GRP_07",
              "STI_Emp_GRP_08",
              "STI_Emp_GRP_09",
              "STI_Emp_GRP_10",
              "STI_Emp_GRP_11",
              "STI_Emp_GRP_12"
            ]
          }
        ]
      },
      "contextData": {
        "campaignId": "597b474bfe305700157895c0",
        "companyId": "5a1157354ad4b87ff889417e",
        "companyName": "KFC",
        "sectionId": "COMPEN_000",
        "grpCode": "",
        "cpyCode": "",
        "ctryCode": "PL",
        "orgSize": null,
        "uniqueIdColumnCode": "YOUR_EEID",
        "industry": {
          "superSector": "CG",
          "sector": "101",
          "subSector": "2670"
        }
      }
    }
  ]
}