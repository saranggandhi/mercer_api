package in.lnt.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lti.mosaic.db.EBDbUtils;
import com.lti.mosaic.db.MysqlConnectionMercer;
import com.lti.mosaic.derby.DerbyLoader;

import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.mysql.MysqlOperations;

@Controller
public class DbQueryController {

	private static final Logger logger = LoggerFactory.getLogger(DbQueryController.class);

	@RequestMapping(value="/queryStatusTable", method=RequestMethod.POST)
	public @ResponseBody String queryStatusTable(HttpServletRequest request, HttpServletResponse response, 
			@RequestHeader ("request_id") String requestId) {

		try {
			if(requestId==null) {
				return "request_id is blank !";
			} else {
				return MysqlOperations.executeQueryAndDisplayWithColNames(requestId);
			}
			
		} catch(Exception e) {
			logger.error(LoggerConstants.LOG_MAXIQAPI + " << queryStatusTable {}", e);
			return "Exception : " + e.getMessage();
		}
	}
}