package in.lnt.pojo;

import java.sql.Timestamp;

/**
 * @author Nikhil Kshirsagar
 * @since 25-09-2017
 * @version 1.0 
 */
public class FileStorageInfo {
	private String srNo;
	private String requestId;
	private Timestamp timeStamp;
	private String fileName;
	private String requestLocale;
	private String requestAgent;
	private String requestIp;

	public String getSrNo() {
		return srNo;
	}
	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getRequestLocale() {
		return requestLocale;
	}
	public void setRequestLocale(String requestLocale) {
		this.requestLocale = requestLocale;
	}
	public String getRequestAgent() {
		return requestAgent;
	}
	public void setRequestAgent(String requestAgent) {
		this.requestAgent = requestAgent;
	}
	public String getRequestIp() {
		return requestIp;
	}
	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}
	
}
