package in.lnt.validations.evaluator;

import java.io.IOException;
import java.rmi.server.Skeleton;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.db.EBDbUtils;
import com.lti.mosaic.function.def.CustomFunctions;
import com.lti.mosaic.parser.driver.ExpressionEvalutionDriver;
import com.lti.mosaic.parser.exception.ExpressionEvaluatorException;
import com.lti.mosaic.parser.exception.FieldNotValidException;

import in.lnt.constants.Constants;
import in.lnt.enums.DataTypes;
import in.lnt.enums.ValidationTypes;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.service.db.DBUtils;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.DataUtility;
import in.lnt.utility.general.JsonUtils;
import net.sf.ehcache.CacheManager;

public class APIExpressionEvaluator {
	private static final Logger logger = LoggerFactory.getLogger(APIExpressionEvaluator.class);
	private ExpressionEvalutionDriver expressionEvalutionDriver = null;
	private  Pattern pattern = null;
	private static ObjectMapper mapper = new ObjectMapper();
	private Map<String, String> columnDataTypeMapping = null;
	public static final DecimalFormat df = new DecimalFormat("#");

	public APIExpressionEvaluator(ExpressionEvalutionDriver expressionEvalutionDriver) {
		this.expressionEvalutionDriver = expressionEvalutionDriver;
	}
	
	public ExpressionEvalutionDriver getExpressionEvaluatorDriver() {
		return expressionEvalutionDriver;
	}

	public void setExpressionEvaluatorDriver(ExpressionEvalutionDriver expressionEvalutionDriver) {
		this.expressionEvalutionDriver = expressionEvalutionDriver;
	}

	public Map<String, String> getColumnDataTypeMapping() {
		return columnDataTypeMapping;
	}

	public void setColumnDataTypeMapping(Map<String, String> columnDataTypeMapping) {
		this.columnDataTypeMapping = columnDataTypeMapping;
	}


	@SuppressWarnings("deprecation")
	public ObjectNode expressionEvaluator(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode, boolean isAggregateRequest,	HashMap<String, String> columnMappingMap,
			ArrayList<JsonNode> columnNodeList , List<String> metaDataColumns) throws IOException {
		String expr = "";
		String result = "";
		ObjectNode obj = null;
		boolean checkBox = false;
		boolean isHiddenQue=false;
		String errorType = "";
		// Start PE- 7056
		ArrayList<String>  metaDataColumnsOnly= (ArrayList<String>) metaDataColumns;
		// End PE- 7056
		String columnCode = "" ;
		if(!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.CODE))){
			columnCode = columnNode.get(Constants.CODE).asText();
		}
		String mappedColumnName = "" ;
		String logErrorString = "";
		
		if(!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.MAPPED_COLUMN_NAME) ) ){
			mappedColumnName = columnNode.get(Constants.MAPPED_COLUMN_NAME).asText();
		}
		
		
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) && 
				!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.QUESTION_TYPE))
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase(Constants.CHECKBOXES)) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) &&
				!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.HIDDEN)) 
				&& columnNode.get(Constants.HIDDEN).asText().equalsIgnoreCase("true")) {
			isHiddenQue = true;
		} 
		
		StringBuilder warnMessage = new StringBuilder("Row id is : ");
		String warnVar = dataMap.get("contextData.uniqueIdColumnCode").asText();
		
		Map<String, Object> paramValues = new HashMap<>();
		Object exprObj = null;
		if (!JsonUtils.isNullOrBlankOrNullNode(validationOBJ) && 
				!JsonUtils.isNullOrBlankOrNullNode(validationOBJ.get(Constants.EXPRESSION))) {

			paramValues = getParamValues(dataMap, validationOBJ, checkBox, columnMappingMap,
											metaDataColumnsOnly,isHiddenQue);
			if (paramValues.containsKey(Constants.EXPRESSION_STRING)) {	// NK
				exprObj = paramValues.get(Constants.EXPRESSION_STRING);
			}
			if (exprObj instanceof String && !StringUtils.isEmpty(exprObj.toString()) && !paramValues.isEmpty()) {	// NK
				expr = exprObj.toString();

			try {
					if(!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.MAPPED_COLUMN_NAME))) {
						
					}
				if (columnCode.length() > 0 &&  columnCode.equalsIgnoreCase(Constants.EMPLOYEE_EEID) 
						&& !StringUtils.isEmpty(mappedColumnName)) {

					if (dataMap != null && (JsonUtils.isNullOrBlankOrNullNode(dataMap.get("YOUR_EEID"))
							|| dataMap.get("YOUR_EEID").asText().length()==0)) {

						return null;
					}
				}
				else if (columnCode.length() > 0 && columnCode.equalsIgnoreCase(Constants.EMPLOYEE_EEID)
						&& !StringUtils.isEmpty(mappedColumnName)) {

					if (dataMap != null
							&& (dataMap.get(mappedColumnName) == null
							|| dataMap.get(mappedColumnName).asText().equals(Constants.BLANK))) {

						return null; 
					}
				}
				result = expressionEvalutionDriver.expressionEvaluatorWithObject(paramValues, expr, false);
				
				//PE-8712 If null is the result for expression then do not show validation object
				// And log the error.
				if( null == result ) {
					 logger.warn( warnMessage.append(null != dataMap.get(warnVar) ? dataMap.get(warnVar).asText():"")
							 .append(", Validation object id is :").append(null != validationOBJ.get("_id")?
									 validationOBJ.get("_id").asText():"")
							 .append(", Erroneous expression is: ").append(expr)
							 .append(", Question code is: ").append(null !=columnNode.get(Constants.CODE)?
							  columnNode.get(Constants.CODE).asText():"").toString());
					 warnMessage.delete(0, warnMessage.length()-1);
					 return null;
				}
			} catch (FieldNotValidException e) {
				logger.error("Exception occured in expressionEvaluator ..{}" , e.getMessage());
				result = e.getMessage();
				result = getMessageForAnErrorType(validationOBJ, ValidationTypes.EXPRESSIONERROR, expr, result);
				obj = prepareOutputJson(validationOBJ, columnNode, expr + result, ValidationTypes.EXPRESSIONERROR,
						columnCode, expr, null, null);
				return obj;
			} catch (ExpressionEvaluatorException e) {
				logger.warn( prepareErrorObjectForLog(dataMap, validationOBJ, columnNode, expr, warnMessage, warnVar, e));
				 warnMessage.delete(0, warnMessage.length()-1);
				return null;
			} catch (Exception e) {
				logger.error("Exception occured in expressionEvaluator ..{}" , e.getMessage());
				result = e.getMessage();
				obj = prepareOutputJson(validationOBJ, columnNode, expr + " " + Constants.ATTENTION + result,
						ValidationTypes.EXPRESSIONERROR, columnCode, expr, null, null);
				return obj;
			}
			if (isAggregateRequest) {
				obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.EXPRESSION, null, null, null,
						null);
				if (StringUtils.isBlank(result) || result.length() < 1) {
					result = "[]";
				}
				obj.put("data", mapper.readTree(result));
				return obj;
			}

			if (validationOBJ.get(Constants.ERROR_TYPE) != null)
				errorType =  validationOBJ.get(Constants.ERROR_TYPE).asText();

			if ((errorType.compareToIgnoreCase(Constants.ERROR) == 0)
					|| (errorType.compareToIgnoreCase(Constants.ALERT) == 0)
					|| (errorType.compareToIgnoreCase(Constants.ENHANCEMENT) == 0)) {
				if (result != null && result.equals("true")) {
					if (columnNode.has(Constants.DIMENSIONS) && !columnNode.get(Constants.DIMENSIONS).isNull()
							&& (dataNode.hasNonNull(columnCode)&& dataNode.get(columnCode).getNodeType() == JsonNodeType.OBJECT)) {
						obj = setValidationsForDimensions(columnNode, dataNode, validationOBJ, result, Constants.ALERT);
					} else
						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.EXPRESSION, null, null,
								null, null); // PE-3711
				}
			} else if (errorType.compareToIgnoreCase(Constants.AUTOCORRECT) == 0) {

				// AdHoc solution for 7241
				if (columnNode.has(Constants.DIMENSIONS) && !columnNode.get(Constants.DIMENSIONS).isNull()
						&& (dataNode.hasNonNull(columnCode) && dataNode
								.get(columnCode).getNodeType() == JsonNodeType.OBJECT)) {
					obj = setValidationsForDimensions(columnNode, dataNode, validationOBJ, result,
							Constants.AUTOCORRECT);
					} else if (columnNode.has(Constants.CODE) &&
							(JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnCode))
									|| (!JsonUtils.isNullOrBlankOrNullNode(
											dataMap.get(columnCode))
											&& !dataMap.get(columnCode).asText()
													.equals(result)))) {
					((ObjectNode) validationOBJ).set(Constants.ORIGINAL_VALUE,
							dataMap.get(columnCode));
					((ObjectNode) (dataNode)).put(columnCode, result);
					if (!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(columnCode))) {
						dataMap.put(columnCode,dataNode.get(columnCode));
					}
					// End PE-7071
					obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.EXPRESSION, null, null,
							null, null);
				}
			}
		}
	}		
		if (null == obj && (null == result || !result.equals(Constants.FALSE))
				&& !errorType.equalsIgnoreCase(Constants.AUTOCORRECT) && !paramValues.isEmpty()) {
			obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.EXPRESSION, null, null, null,
					null);
		}
		return obj;
	}

	/**
	 * @param dataMap
	 * @param validationOBJ
	 * @param columnNode
	 * @param expr
	 * @param warnMessage
	 * @param warnVar
	 * @param e
	 * @return
	 */
	public String prepareErrorObjectForLog(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, String expr, StringBuilder warnMessage, String warnVar,
			ExpressionEvaluatorException e) {
		return warnMessage.append(null != dataMap.get(warnVar) ? dataMap.get(warnVar).asText():"")
				 .append(", Validation object id is :").append(null != validationOBJ.get("_id")?
						 validationOBJ.get("_id").asText():"")
				 .append(", Erroneous expression is: ").append(expr)
				 .append(", Question code is: ").append(null !=columnNode.get(Constants.CODE)?
				  columnNode.get(Constants.CODE).asText():"")
				 .append(", Reason : ").append(e.getMessage()).toString();
	}

	public ObjectNode checkMandatoryFields(HashMap<String, JsonNode> dataMap, String key, String dataType,
			String mappedColumn) throws Exception {
		if ((dataMap.get(key) == null || dataMap.get(key).asText().isEmpty())
				&& (dataMap.get(mappedColumn) == null || dataMap.get(mappedColumn).asText().isEmpty())) {
			return prepareOutputJson(null, null, null, ValidationTypes.REQUIRED, key, null, null, null);
		}
		return checkDataType(dataMap, key, dataType, mappedColumn);
	}

	/*
	 * For Cross Section
	 */
	public ObjectNode expressionEvaluator(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode, Map<String, Integer> cscDuplicateChkMap,
			ArrayList<JsonNode> columnNodeList, List<String> metaDataColumns, int endPoint) throws CustomStatusException, Exception {
		ArrayList<String> paramValueList = null;
		String expr = "";
		String result = "";
		ObjectNode obj = null;
		boolean checkBox = false;
		String fieldArr[] = null;
		String prefix = Constants.THIS;
		String patternString = "";
		ArrayList<String> dimList = null;
		JsonNode dimNode = null;
		boolean isHiddenQue=false;
		// Start PE- 7056
		ArrayList<String>  metaDataColumnsOnly= (ArrayList<String>) metaDataColumns;
		boolean isExpressionFlag = true;
		boolean thisAndContextFlag = false;
		// End PE- 7056
		
		if (columnNode != null && columnNode.get(Constants.QUESTION_TYPE) != null
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase(Constants.CHECKBOXES)) {
			checkBox = true;
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		} else {
			pattern = Pattern.compile(Constants.EXP_PATTERN);
		}
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) &&
				!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.HIDDEN)) 
				&& columnNode.get(Constants.HIDDEN).asText().equalsIgnoreCase("true")) {
			isHiddenQue = true;
		} 
		Map<String, Object> paramValues = new HashMap<>();
		if (validationOBJ != null && validationOBJ.get(Constants.EXPRESSION) != null
				&& !validationOBJ.get(Constants.EXPRESSION).asText().equals(Constants.BLANK)) {
			// PE7044 Blank expression clause is added.
			expr = validationOBJ.get(Constants.EXPRESSION).asText().replaceAll(Constants.CURLY_BRACES, "");
			patternString = expr;
			
			if (expr != null && expr.contains(Constants.BOOL_IN_LOOKUP)) {
				patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
			}
			
			Matcher matcher = pattern.matcher(patternString);
			List<String> fields = new ArrayList<>();
			while (matcher.find()) {
				fields.add(matcher.group().replace(Constants.THIS, "").trim());
			}

			if( !fields.isEmpty() ) {  	
				thisAndContextFlag = true;
			}
			if ( thisAndContextFlag ) {
					if ( isHiddenQue || (!metaDataColumnsOnly.isEmpty() &&
							chekElementExist(metaDataColumnsOnly,fields)) ) { 
						isExpressionFlag = true;
					}else{
						isExpressionFlag = false;
					}
			 }
			
			if ( isExpressionFlag ) {  //PE- 7056
				for (String field : fields) {
					if (field != null && field.trim().contains(Constants.CONTEXTDATAKEY)) {
						prefix = prefix.replace(Constants.THIS, "");
					}
					if (checkBox) {
						fieldArr = extractFields(field.trim());
						if (fieldArr != null) {
							      paramValues.put(prefix + field.trim(), (dataMap.get(fieldArr[0]) == null)
								? (dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD+ fieldArr[0].trim()) == null ? Constants.NULL
								: findInArray(dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldArr[0]), fieldArr[1]))
								: findInArray(dataMap.get(fieldArr[0]), fieldArr[1]));
						} else {
								paramValues.put(prefix + field.trim(), (dataMap.get(field.trim()) == null)
								? (dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD  + field.trim()) == null ? Constants.NULL
								: getContextDataValues(dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD +
										field.trim()).asText() .replace(",", "")))
									: getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(),Constants.DATA_TYPE),
											dataMap.get(field.trim()).asText().replace(",", "")));
						}
					} else {
						String tmp = null;
						 if (!dataMap.containsKey(field.trim()) && !isHiddenQue) {												// PE-7506 						
							if (dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD  + field.trim()) == null) {
								if (cscDuplicateChkMap != null && cscDuplicateChkMap.containsKey(field.trim())) {
									paramValueList = new ArrayList<>();
									paramValueList.add("" + cscDuplicateChkMap.get(field.trim()));
									paramValueList.add("" + field.trim());
									paramValueList.add("" + expr);
									throw new CustomStatusException(Constants.HTTPSTATUS_400,
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_048),
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_048 + Constants.UNDERSCORE_PARAMS),
											paramValueList);
								} else {
									paramValueList = new ArrayList<>();
									paramValueList.add(field.trim());
									paramValueList.add(expr);
									throw new CustomStatusException(Constants.HTTPSTATUS_400,
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_007),
											Cache.getPropertyFromError(ErrorCacheConstant.ERR_007 + Constants.UNDERSCORE_PARAMS),
											paramValueList);
								}
							} else {
								tmp = getContextDataValues(
										dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD  
												+ field.trim()).asText().replace(",", ""));
							}
						} else {
							String val = !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(field.trim()))?dataMap.get(field.trim()).asText():"";
							tmp = getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(field.trim(), Constants.DATA_TYPE),
									val.replace(",", ""));
							val=null;
						}
						paramValues.put(prefix + field.trim(), tmp);
					}
					prefix = Constants.THIS;
				}
				try {
					result = expressionEvalutionDriver.expressionEvaluatorWithObject(paramValues, expr, false);
	
				} catch (ExpressionEvaluatorException e) {
					logger.warn("Erroneous expression is: {}", expr +", Reason {}"+e.getMessage() );
					return null;
				}catch (Exception e) {
					logger.error("Excepion occured in expressionEvaluator..{}" , e.getMessage());
					paramValueList = new ArrayList<>();
					paramValueList.add(e.getMessage());
					if ( e.getCause().getMessage().indexOf("org.antlr.v4.runtime") == -1 ) {
						obj = null;
					}else {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_040),
							in.lnt.utility.general.Cache
									.getPropertyFromError(ErrorCacheConstant.ERR_040 + Constants.UNDERSCORE_PARAMS),
							paramValueList);
					}
				}
				String errorType = "";
				if (validationOBJ.get(Constants.ERROR_TYPE) != null)
					errorType = validationOBJ.get(Constants.ERROR_TYPE).asText();
				if ((errorType.compareToIgnoreCase(Constants.ERROR) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ALERT) == 0)
						|| (errorType.compareToIgnoreCase(Constants.ENHANCEMENT) == 0)) {
					if (result != null && result.equals("true")) {
	
						// Implementation start PE : 5414
	
						// PE-8269 Prasad Mail : MDA not applicable for crossectionchecks
						if (endPoint==Constants.ENDPOINT_PERFORM_DEFAULT_ACTIONS // PE-8397
								&& validationOBJ.get(Constants.MDA) != null
								&& !validationOBJ.get(Constants.MDA).asText().equals("")
								&& validationOBJ.get(Constants.ERROR_TYPE).asText().equals(Constants.ERROR)) {
							if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.EXECLUE_ROW)) {
								List<String> columnNodeList_Section = new ArrayList<>();
								for(int index = 0; index < columnNodeList.size(); index++){
									columnNodeList_Section.add(columnNodeList.get(index).get(Constants.CODE).asText());
								}
								if(columnNodeList_Section != null && columnNodeList_Section.contains(Constants.EXCLUDE_FLAG)){//PE-7771
									((ObjectNode) (dataNode)).put(Constants.EXCLUDE_FLAG, Constants.ANSWER_YES);
									return obj;
								}
							} else if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.CLEAR_VALUE)) {
								((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), "");
								return obj;
							} else {// 7044 condition added
								// 7044 condition added
								String responseData = expressionEvaluatorForMDA(dataMap, validationOBJ, columnNode,
										cscDuplicateChkMap, metaDataColumnsOnly);
								if(null != responseData && !responseData.equals(Constants.INVALID_EXP)) {
									if (columnNode.get(Constants.DIMENSIONS) != null) {
										dimList = APIExpressionEvaluator
												.extractDimensions(columnNode.get(Constants.DIMENSIONS));
										dimNode = mapper.createObjectNode();
										for (String dim : dimList) {
											((ObjectNode) dimNode).put(dim, responseData);
										}
										((ObjectNode) (dataNode)).set(columnNode.get(Constants.CODE).asText(), dimNode);
									}
		
									else {
										((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), responseData);
									}
		
									return obj;
								}
								//PE-8609
								if( null == responseData ) {
									return null;
								}
							}
						}
	
					
						// Implementation end PE : 5414
	
						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.EXPRESSION, null, null,
								null, null); // PE-3711
					} else if (result != null && result.equals("false")) {
					} else {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_005));
					}
				} else if (errorType.compareToIgnoreCase("AUTOCORRECT") == 0) {
					if (columnNode.has(Constants.CODE)
						&& (JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnNode.get(Constants.CODE).asText()))
					    || (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(columnNode.get(Constants.CODE).asText()))
						&& !dataMap.get(columnNode.get(Constants.CODE).asText()).asText() .equals(result)))) {
								((ObjectNode) validationOBJ).set(Constants.ORIGINAL_VALUE,dataMap.
								get(columnNode.get(Constants.CODE).asText()));
						((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), result);
						obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.EXPRESSION, null, null,
								null, null);
					}
				}
	
			}
		}		
		return obj;
	}

	public ObjectNode checkMandatoryFields(HashMap<String, JsonNode> dataMap, String key, String dataType)
			throws Exception {
		if (dataMap.get(key) == null || dataMap.get(key).asText().isEmpty()) {
			return prepareOutputJson(null, null, null, ValidationTypes.REQUIRED, key, null, null, null);
		}
		return checkDataType(dataMap, key, dataType);
	}

	public ObjectNode checkDataType(HashMap<String, JsonNode> dataMap, String key, String dataType) throws Exception {
		// logger.info(String.format("%s %s", Constants.INTIATE
		// ,Constants.LOG_CHECKDATATYPE));
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);
		ObjectNode obj = null;
		if (EnumUtils.isValidEnum(DataTypes.class, String.valueOf(dataType).toUpperCase())) {
			String value = String.valueOf(dataMap.get(key).asText()).replace(",", "");
			switch (DataTypes.valueOf(String.valueOf(dataType.toUpperCase()))) {
			case STRING:
				break;
			case INTEGER:
			case INT:
				if (((StringUtils.isEmpty(value)) || (value == Constants.NULL)) || (INT_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DATATYPE, key, value, dataType, null);
				}
				break;
			case DOUBLE:
				if (((StringUtils.isEmpty(value)) || (value == Constants.NULL)) || (DOUBLE_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DATATYPE, key, value, dataType, null);
				}
				break;
			case DATE:
				break;
			case BOOLEAN:
				break;
			case LONG:
				if (((StringUtils.isEmpty(value)) || (value == Constants.NULL)) || (LONG_PATTERN.matcher(value).matches())) {
				} else {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DATATYPE, key, value, dataType, null);
				}
				break;
			}
		}
		// logger.info(String.format("%s %s", Constants.EXIT
		// ,Constants.LOG_CHECKDATATYPE));
		return obj;
	}


	public ObjectNode checkDataType(HashMap<String, JsonNode> dataMap, String key, String dataType, String mappedColumn)
			throws Exception {
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);
		ObjectNode obj = null;
		String value = "";
		if (EnumUtils.isValidEnum(DataTypes.class, String.valueOf(dataType).toUpperCase())) {
			if (key != null && !key.equals("")) {
				value = String.valueOf(dataMap.get(key).asText()).replace(",", "");
			} else {
				value = String.valueOf(dataMap.get(mappedColumn).asText()).replace(",", "");
			}
			switch (DataTypes.valueOf(String.valueOf(dataType.toUpperCase()))) {
			case STRING:
				break;
			case INTEGER:
			case INT:
				if ( !((StringUtils.isEmpty(value)) || (value == Constants.NULL) || (INT_PATTERN.matcher(value).matches())) ) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DATATYPE, key, value, dataType, null);
				}
				break;
			case DOUBLE:
				if ( !((StringUtils.isEmpty(value)) || (value == Constants.NULL) || (DOUBLE_PATTERN.matcher(value).matches()))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DATATYPE, key, value, dataType, null);
				}
				break;
			case DATE:
				break;
			case BOOLEAN:
				break;
			case LONG:
				if (!((StringUtils.isEmpty(value)) || (value == Constants.NULL) || (LONG_PATTERN.matcher(value).matches()))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.DATATYPE, key, value, dataType, null);
				}
				break;
			}
		}
		return obj;
	}

	public ObjectNode checkValidationTypeOneOf(HashMap<String, JsonNode> dataMap, String key, JsonNode node,
			JsonNode validationNode) {
		ObjectNode obj = null;
		Pattern INT_PATTERN = Pattern.compile("^([+-]?[0-9]\\d*|0)$");
		Pattern DOUBLE_PATTERN = Pattern.compile("\\d+\\.\\d+");
		Pattern LONG_PATTERN = Pattern.compile("^-?\\d{1,19}$");
		String value = dataMap.get((node.get("code").asText())).asText();
		JsonNode valuesNode = validationNode.get(Constants.VALUES);
		ArrayList<Object> valueList = new ArrayList<>();
		StringBuilder values = new StringBuilder("");

		if (valuesNode != null) {

			for (int i = 0; i < valuesNode.size(); i++) {
				valueList.add(valuesNode.get(i));
				if (i == 0)
					values = values.append(valuesNode.get(i).asText());
				else
					values = values.append(",").append(valuesNode.get(i).asText());
			}

		}
		
		String modifiedValue = String.valueOf(value).replace(",", "");
		switch (validationNode.get(Constants.VALUES).get(0).getNodeType()) {
		case NUMBER:
			if (DOUBLE_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Double.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.ONEOF, key, value, null, values.toString());
				}
			} else if (INT_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Integer.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.ONEOF, key, value, null, values.toString());
				}
			} else if (LONG_PATTERN.matcher(modifiedValue).matches()) {
				if (!valueList.contains(Integer.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.ONEOF, key, value, null, values.toString());
				}

			} else {

				obj = prepareOutputJson(null, null, null, ValidationTypes.ONEOF, key, value, null, values.toString());
			}

			break;
		case STRING:
			if (!StringUtils.isBlank(value)) {
				if (!valueList.contains(String.valueOf(value))) {
					obj = prepareOutputJson(null, null, null, ValidationTypes.ONEOF, key, value, null, values.toString());
				}
			} else {
				obj = prepareOutputJson(null, null, null, ValidationTypes.ONEOF, key, value, null, values.toString());
			}
			break;
			
		 default :break;	

		}
		return obj;
	}

	private String[] extractFields(String field){
		String arr[] = null;
		if (field != null && field.length() > 0 && StringUtils.contains(field, "['")
				&& StringUtils.contains(field, "']")) {
			// ArrayUtils
			arr = StringUtils.split(field, "['");

		}
		return arr;
	}

	private String findInArray(JsonNode node, String value){

		boolean returnValue = false;
		if (null != node && node.getNodeType() == JsonNodeType.ARRAY) {
			Iterator<JsonNode> it = node.iterator();
			while (it.hasNext()) {
				returnValue = it.next().asText().equalsIgnoreCase(value);
				if (returnValue)
					break;
			}
		} else {
			if (null != node) {
				String arr[] = node.asText().split(",");
				if (arr != null && arr.length > 0) {
					for (int i = 0; i < arr.length; i++) {
						if (arr[i].equalsIgnoreCase(value))
							returnValue = true;
					}
				}
			}

		}
		return returnValue + "";
	}

	/*
	 * NOTE : Use this method to : Generate message(or result) for the
	 * prepareOutputJson() method depending upon the "errorType"
	 */
	public static String getMessageForAnErrorType(JsonNode validationOBJ, ValidationTypes errorType, String expr,
			String result){
		String message = null;
		if (errorType != null) {
			switch (errorType) {
			case EXPRESSIONERROR:
				// PE-6660 : If "message" is provided by user in validationOBJ,
				// use it.
				// Else display default message.
				if (!JsonUtils.isNullOrBlankOrNullNode(validationOBJ.get(Constants.MESSAGE))) {
					if (StringUtils.isBlank(validationOBJ.get(Constants.MESSAGE).asText())) {
						message = result;
					} else {
						message = validationOBJ.get(Constants.MESSAGE).asText();
					}
				}
				break;

			case EEIDAUTOCORRECT:
				message = "Missing employee Id geneated:" + result;
				break;

			default:
				message = expr + " " + "ATTENTION: " + result;
				break;

			}
		} else {
			message = expr + " " + "ATTENTION: " + result;
		}
		return message;
	}

	public static ObjectNode prepareOutputJson(JsonNode validationOBJ, JsonNode columnNode, String result,
			ValidationTypes errorType, String ky, String value, String datatype, String values){

		/*
		 * NOTE : In order to create result, use getMessageForAnErrorType()
		 * written above this method.
		 */

		ObjectNode validation = mapper.createObjectNode();
		JsonNode paramNode = mapper.createObjectNode();
		String message = null;
		if (errorType != null) {
			switch (errorType) {
			case DATATYPE:
				validation.put(Constants.FIELD, ky);
				
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_045));
				((ObjectNode) paramNode).put("key", ky);
				((ObjectNode) paramNode).put(Constants.VALUE, value);
				((ObjectNode) paramNode).put(Constants.DATA_TYPE, datatype);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;
			case REQUIRED:
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_041));
				((ObjectNode) paramNode).put("key", ky);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;
			case ONEOF:
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.MESSAGE,
						value + " is not a valid value for this field.  Please choose from " + values);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_042));
				((ObjectNode) paramNode).put(Constants.VALUE, value);
				((ObjectNode) paramNode).put(Constants.VALUES, values);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;
			case EXPRESSION:
				Iterator<String> it = validationOBJ.fieldNames();
				String key = "";
				while (it.hasNext()) {
					key = it.next();
					if(key.equals(Constants.EXPRESSION))
					{
						continue;
					}
					validation.put(key, validationOBJ.get(key).asText());
				}
				// PE-7071 : Blank CODE will be accepted.
				String field = JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.CODE))
						? (columnNode.get(Constants.CODE) == null ? null : "")
						: columnNode.get(Constants.CODE).asText();
				validation.put(Constants.FIELD, field);
				break;

			case DATATYPEERROR:
				if (null != validationOBJ) {
					Iterator<String> itr = validationOBJ.fieldNames();
					String keyName = "";
					while (itr.hasNext()) {
						keyName = itr.next();
						if(keyName.equals(Constants.EXPRESSION))
						{
							continue;
						}
						validation.put(keyName, validationOBJ.get(keyName).asText());
					}
				}
				validation.put(Constants.FIELD, columnNode.get(Constants.CODE).asText());
				validation.put(Constants.VALIDATION_TYPE, "dataTypeCheck");
				validation.put(Constants.ERROR_TYPE, Constants.ERROR);
				validation.put(Constants.ID,Constants.DATA_TYPE + StringUtils.capitalize(datatype));
				validation.put(Constants.MESSAGE_KEY,
						result);
				((ObjectNode) paramNode).put(Constants.VALUE, value);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;

			case EXPRESSIONERROR:
				Iterator<String> expIt = validationOBJ.fieldNames();
				String expKey = "";
				while (expIt.hasNext()) {
					expKey = expIt.next();
					validation.put(expKey, validationOBJ.get(expKey).asText());
				}
				validation.put(Constants.FIELD, ky);
				if (!JsonUtils.isNullOrBlankOrNullNode(validationOBJ.get(Constants.MESSAGE))
						&& StringUtils.isBlank(validationOBJ.get(Constants.MESSAGE).asText())) {
					validation.put(Constants.MESSAGE_KEY,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_044));
					((ObjectNode) paramNode).put("result", result);
					validation.set(Constants.MESSAGE_PARAMS, paramNode);
				} else {
					validation.put(Constants.MESSAGE, result);
				}
				break;
			case EEIDAUTOCORRECT:
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.EXPRESSION);
				validation.put(Constants.ERROR_TYPE, Constants.AUTOCORRECT);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_039));
				((ObjectNode) paramNode).put("eeid", result);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				validation.put(Constants.ORIGINAL_VALUE, Constants.BLANK);
				break;

			case RANGE:
				Iterator<String> it2 = validationOBJ.fieldNames();
				while (it2.hasNext()) {
					key = it2.next();
					if (validationOBJ.get(key).getNodeType() == JsonNodeType.ARRAY)
						validation.set(key, validationOBJ.get(key));
					else
						validation.put(key, validationOBJ.get(key).asText());
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.VALIDATION_RANGE);
				validation.put(Constants.ERROR_TYPE, value);
				validation.put(Constants.MESSAGE_KEY, result);
				validation.put("category", "input_data");
				if (!values.equals(Constants.BLANK))
					validation.put(Constants.DIMENSION, values);
				break;
			case RANGEVALIDATIONREFTABLE:

				Iterator<String> it1 = validationOBJ.fieldNames();
				while (it1.hasNext()) {
					key = it1.next();
					if (validationOBJ.get(key).getNodeType() == JsonNodeType.ARRAY)
						validation.set(key, validationOBJ.get(key));
					else
						validation.put(key, validationOBJ.get(key).asText());
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.ERROR_TYPE, value);
				validation.put(Constants.MESSAGE_KEY, result);
				if (null != values && !values.equals(Constants.BLANK) )
					validation.put(Constants.DIMENSION, values);
				break;
			case ELIGIBILITY:
				validation = (ObjectNode) validationOBJ;
				break;

			case DROPDOWN:
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.ERROR_TYPE, Constants.ERROR);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_053));
				((ObjectNode) paramNode).put(Constants.VALUES, values);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				break;

			case SEQUENTIALCHECK:
				it = validationOBJ.fieldNames();
				while (it.hasNext()) {
					key = it.next();
					if (key.equalsIgnoreCase("dependentColumns")) {
						validation.put(key, validationOBJ.get(key));
					} 
					else if(key.equalsIgnoreCase(Constants.EXPRESSION)){
						continue;
					}
					else {
						validation.put(key, validationOBJ.get(key).asText());
					}
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.VALIDATION_TYPE, Constants.SEQUENTIAL_CHECK);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_054));
				((ObjectNode) paramNode).put("column", ky);
				((ObjectNode) paramNode).put("prevColumn", value);
				validation.set(Constants.MESSAGE_PARAMS, paramNode);
				validation.put(Constants.ERROR_TYPE, Constants.ERROR);
				if (!values.equals(Constants.BLANK))
					validation.put(Constants.DIMENSION, values);
				break;
			case PREDEFINEDVALIDATIONS:
				if (!JsonUtils.isNullOrBlankOrNullNode(validationOBJ)) {
					Iterator<String> predefinedIter = validationOBJ.fieldNames();
					String predefinedkey = "";
					while (predefinedIter.hasNext()) {
						predefinedkey = predefinedIter.next();
						validation.put(predefinedkey, validationOBJ.get(predefinedkey).asText());
					}
				}
				validation.put(Constants.FIELD, ky);
				validation.put(Constants.MESSAGE_KEY,
						in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_029));
				if (!JsonUtils.isNullOrBlankOrNullNode(columnNode)) {
					validation.put(Constants.DIMENSION, columnNode.asText());
				}
				break;
			default:
				break;
			}
		} else {
			if (result != null && result.length() > 0) {
				validation.put(Constants.MESSAGE, result);
			}
		}
		return validation;
	}

	public Map<String, Object> getParamValues(Map<String, JsonNode> dataMap, JsonNode validationOBJ,boolean checkBox,	// NK
			Map<String, String> columnMappingMap,  
			List<String> metaColnsOnly,boolean isHiddenQue)
			{
		String patternString = "";
		String expr = "";
		String prefix = Constants.THIS;
		String[] fieldArr = null;
		boolean isExpressionFlag = true;
		Map<String, Object> paramValues = new HashMap<>();	// NK
		expr = validationOBJ.get(Constants.EXPRESSION).asText();// .replaceAll(Constants.CURLY_BRACES,
		boolean thisAndContextFlag = false;	 // PE - 7056

		patternString = expr;
		if (expr != null && expr.contains(Constants.BOOL_IN_LOOKUP)) {
			patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
		}
		Matcher matcher = pattern.matcher(patternString);
		List<String> fields = new ArrayList<>();
		while (matcher.find()) {
			fields.add(matcher.group().replace(Constants.THIS, "").trim());
		}
		// PE - 7056	
		if( !fields.isEmpty() ) {  	
			thisAndContextFlag = true;
		}
		if ( thisAndContextFlag ) {
				if ( (!metaColnsOnly.isEmpty() &&  chekElementExist(metaColnsOnly,fields)) ||  isHiddenQue) { 
					isExpressionFlag = true;
				}else{
					isExpressionFlag = false;
				}
		 }
		if ( isExpressionFlag ) {
			String fieldtrim = "";
			JsonNode dataMap_field_node = null;
		for (String field : fields) {
			if (null != field) {
				fieldtrim = field.trim(); 
				if (fieldtrim.contains(Constants.CONTEXTDATAKEY)) {
					prefix = prefix.replace(Constants.THIS, "");
				}
				dataMap_field_node = dataMap.get(fieldtrim);
			}
			if (checkBox) {
				fieldArr = extractFields(fieldtrim);
				if (fieldArr != null) {
					paramValues.put(prefix + fieldtrim,
							(dataMap.get(fieldArr[0]) == null)
									? (dataMap.get(columnMappingMap.get(fieldArr[0].trim())) == null
										? (dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD 
										  + fieldArr[0].trim()) == null ? Constants.NULL
											 : findInArray(dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldArr[0]),
															fieldArr[1])) 	: Constants.NULL)
									: findInArray((dataMap.get(fieldArr[0].trim())), fieldArr[1]));
				} else {
					//// made it data type independent
					paramValues
							.put(prefix + fieldtrim,
									(JsonUtils.isNullOrBlankOrNullNode(dataMap_field_node))
											? (dataMap.get(columnMappingMap.get(fieldtrim)) == null
													? (dataMap
													  .get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldtrim) == null ? Constants.NULL : // To
														 getContextDataValues(getContextDataValues(dataMap
															.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldtrim)
															  .asText().replace(",", ""))))	: getDataTypeBasedValue(
															columnDataTypeMapping.getOrDefault(
																	columnMappingMap.get(fieldtrim), Constants.DATA_TYPE),
															dataMap.get(columnMappingMap.get(fieldtrim)).asText()))
											: getDataTypeValues(fieldtrim, dataMap_field_node)//PE-7440 Method Extracted 

					);
				}
			} else {
				paramValues
						.put(prefix + fieldtrim,
								(JsonUtils
										.isNullOrBlankOrNullNode(
												dataMap_field_node))
														? (dataMap.get(columnMappingMap.get(fieldtrim)) == null
																? (dataMap.get(
																		Constants.OTHER_SECTIONSDATA_PERIOD + fieldtrim) == null
																				? Constants.NULL
																				: dataMap
																						.get(Constants.OTHER_SECTIONSDATA_PERIOD
																								+ fieldtrim)
																						.asText().replace(",", ""))
																: getDataTypeBasedValue(
																		columnDataTypeMapping.getOrDefault(
																				columnMappingMap.get(fieldtrim),
																				Constants.DATA_TYPE),
																		dataMap.get(columnMappingMap.get(fieldtrim))
																				.asText()))
														: getDataTypeValues(fieldtrim, dataMap_field_node));//PE-7440 Method Extracted 
			}
			prefix = Constants.THIS;
		}
		// Add expression
				paramValues.put(Constants.EXPRESSION_STRING, expr);
		}else {	
			paramValues.clear();
		}
		
		return paramValues;
	}

	/**
	 * @param field
	 * @param dataMapField
	 * @return
	 */
	//PE-7440
	protected Object getDataTypeValues(String field, JsonNode dataMapField) {
		
		List <String> dataMapList = new ArrayList<>();
		if( dataMapField.isArray()) {
			for (int i=0; i < dataMapField.size(); i++) {
				dataMapList.add("\""+dataMapField.get(i).asText()+"\"");	// PE-7795 (FIND_IN_SET fails for variable array)	// NK
			}	
			return dataMapList;
		}else {
			return getDataTypeBasedValue(
				columnDataTypeMapping.getOrDefault(field.trim(), Constants.DATA_TYPE),
				dataMapField.asText().replace(",", ""));
		}
	}

	public String getDataTypeBasedValue(String type, String value) {
		try {
			if (!StringUtils.isBlank(value)) {
				switch (type) {
				case "int":
					return value;
				case "double":
					return df.format(Double.valueOf(value));
				case "text":
				case "string":
					return "\"" + value + "\"";
				case "boolean":
					return value;
				case "date":
					return "\"" + value + "\"";
				case Constants.DATA_TYPE:
					return getContextDataValues(value);
				default:
					break;
				}
			} else {
				return Constants.NULL;
			}
		} catch (Exception e) {
			return Constants.NULL;
		}
		return value;
	}

	public String getContextDataValues(String value) {
		try {
			if (!StringUtils.isBlank(value)) {

				// Commenting for PE-7773
					value = "\"" + value + "\""; // Modified for PE: 7254
					return value;
			} else {
				return Constants.NULL;
			}
		} catch (Exception e) {
			return Constants.NULL;
		}
	}

	@SuppressWarnings("unlikely-arg-type")
	public ArrayList<ObjectNode> prepareRangeObject(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, JsonNode dataNode) {

		ObjectNode obj = null;
		String paramValue = "";
		String warning = null;
		String warningMsg = null;
		JsonNode code = null;
		ArrayList<ObjectNode> objList = new ArrayList<>();
		String value = "";
		HashMap<String, String> paramValueMap = null;
		
		String minError = validationOBJ.has(Constants.MINIMUM_ERROR)
				? validationOBJ.get(Constants.MINIMUM_ERROR).asText() : null;
		String maxError = validationOBJ.has(Constants.MAXIMUM_ERROR)
				? validationOBJ.get(Constants.MAXIMUM_ERROR).asText() : null;
		String minAlert = validationOBJ.has(Constants.MINIMIUM_ALERT)
				? validationOBJ.get(Constants.MINIMIUM_ALERT).asText() : null;
		String maxAlert = validationOBJ.has(Constants.MAXIMUM_ALERT)
				? validationOBJ.get(Constants.MAXIMUM_ALERT).asText() : null;
		code = columnNode.get(Constants.CODE);

		if (code != null && columnNode.get(Constants.DIMENSIONS) != null) {
			paramValueMap = prepareParamValuesMap(code, columnNode.get(Constants.DIMENSIONS), dataMap);
		} else if (code != null) {
			paramValueMap = new HashMap<>();
			if (null != dataMap && !JsonUtils.isNullOrBlankOrNullNode(code)
					&& !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code.asText()))) {
				paramValue = dataMap.get(code.asText()).asText();
				paramValueMap.put(Constants.BLANK, paramValue);
			} else if (!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.MAPPED_COLUMN_NAME)) && !JsonUtils
					.isNullOrBlankOrNullNode(dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()))) {
				paramValue = dataMap.get(columnNode.get(Constants.MAPPED_COLUMN_NAME).asText()).asText();
				paramValueMap.put(Constants.BLANK, paramValue);

			}
		}

		if (paramValueMap != null && paramValueMap.size() > 0) {
			for (Map.Entry<String, String> entry : paramValueMap.entrySet()) {
				warning=null;
				warningMsg = null;
				value = entry.getValue();
				if (JsonUtils.isStringExist(value)) {
				if (DataUtility.isStringNotNullAndBlank(minError) && !StringUtils.isEmpty(value)
						&& Double.parseDouble(value) < Double.parseDouble(minError)) {
					warning = Constants.ERROR;
					// "minError"
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_024); 
				}
				if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxError)
						&& Double.parseDouble(value) > Double.parseDouble(maxError)) {
					warning = Constants.ERROR;
					// "minError"
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_027); 

				}
				if (null == warningMsg && DataUtility.isStringNotNullAndBlank(minAlert)
						&& Double.parseDouble(value) < Double.parseDouble(minAlert)) {
					warning = Constants.ALERT;
					// "minError"
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_025); 
				}
				if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxAlert)
						&& Double.parseDouble(value) > Double.parseDouble(maxAlert)) {
					warning = Constants.ALERT;
					// "minError"
					warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_026); 
				}
				}	
				if (warning != null) {
					obj = prepareOutputJson(validationOBJ, columnNode, warningMsg, ValidationTypes.RANGE, code.asText(),
							warning, null, entry.getKey());
					objList.add(obj);
				}
			}
		}
		return objList;
	}

	public ObjectNode setValidationsForDimensions(JsonNode columnNode, JsonNode dataNode, JsonNode validationOBJ,
			String result, String type) {
		ObjectNode obj = null;

		JsonNode dimensionNode = dataNode.path(columnNode.get(Constants.CODE).asText()).deepCopy();
		for (JsonNode jsonNode : columnNode.get(Constants.DIMENSIONS)) {
			// Matching dimension Code within Expression (As no column/sequence
			// is present for dimensional Expression execution)
			try {
				if (validationOBJ.get(Constants.EXPRESSION).asText().contains(jsonNode.asText())) {
					switch (type) {
					case Constants.ERROR:
					case Constants.ENHANCEMENT:
					case Constants.ALERT:
						((ObjectNode) validationOBJ).put("dimension", jsonNode.asText());
						obj = prepareOutputJson(validationOBJ, columnNode, null, ValidationTypes.EXPRESSION, null, null,
								null, null);
						break;
					case Constants.AUTOCORRECT:
						if (!dimensionNode.get(jsonNode.asText()).asText().equals(result)) {
							((ObjectNode) validationOBJ).set("OriginalValue", dimensionNode.get(jsonNode.asText()));
							((ObjectNode) (dimensionNode)).put(jsonNode.asText(), result);
							((ObjectNode) validationOBJ).put("dimension", jsonNode.asText());
							obj = prepareOutputJson(validationOBJ, columnNode, result, ValidationTypes.EXPRESSION, null,
									null, null, null);
						}
						break;
					default : break;	
					}
				}
			} catch (Exception e) {
				logger.info(e.getMessage());
			}
		}
		((ObjectNode) (dataNode)).set(columnNode.get(Constants.CODE).asText(), dimensionNode);
		return obj;
	}

	// PE - 5568
	public ArrayNode checkValidity(JsonNode validationOBJ, JsonNode columnNode,
			JsonNode dataNode, boolean isMDARequest,List<String> metaDataColumnsOnly) {
		ArrayNode arr = mapper.createArrayNode();
		JsonNode paramNode = mapper.createObjectNode();
		if (validationOBJ != null && validationOBJ.has(Constants.DEPENDENT_COLUMNS)
				&& validationOBJ.get(Constants.DEPENDENT_COLUMNS) != null) {
			JsonNode depedentColumns = validationOBJ.get(Constants.DEPENDENT_COLUMNS);
			boolean performMDAAction = false;
			
			if (!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(columnNode.get(Constants.CODE).asText())) && 
					(dataNode.get(columnNode.get(Constants.CODE).asText()).asText()).equals(Constants.ANSWER_YES)) {
				for (JsonNode depedentColumn : depedentColumns) {
					if ((JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
							|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), Constants.NULL)
									|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) 
							&& metaDataColumnsOnly.contains(depedentColumn.asText())) {
						if (!isMDARequest) {
							ObjectNode errorObject = mapper.createObjectNode();
							errorObject.put(Constants.VALIDATION_TYPE,
									validationOBJ.get(Constants.VALIDATION_TYPE).asText());
							errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
							errorObject.put(Constants.FIELD, depedentColumn.asText());
							
							errorObject.put(Constants.MESSAGE_KEY,
									in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_058));
							((ObjectNode) paramNode).put(Constants.DISPLAYLABEL,
									JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
											: columnNode.get(Constants.DISPLAYLABEL).asText());
							errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
							errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.ELIGIBILITY, null,
									null, null, null);
							arr.add(errorObject);
						}
					} else {
						continue;
					}
				}

			} else if (!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(columnNode.get(Constants.CODE).asText())) && 
					(dataNode.get(columnNode.get(Constants.CODE).asText()).asText()).equals(Constants.ANSWER_NO)) {
				// Hierarchy : No Answer, 0, Some Answer

				for (JsonNode depedentColumn : depedentColumns) {
					// Blank / null
					if (JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
							|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), Constants.NULL)
									|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) {
						continue;
					} 
					// zero
					else if (null != dataNode.get(depedentColumn.asText())
							&& dataNode.get(depedentColumn.asText()).asText().equals("0")) {
						((ObjectNode) (dataNode)).put(depedentColumn.asText(), "");
					} else {
						if (!isMDARequest) {
							ObjectNode errorObject = mapper.createObjectNode();

							errorObject.put(Constants.ERROR_TYPE, Constants.ERROR);
							errorObject.put(Constants.VALIDATION_TYPE,
									validationOBJ.get(Constants.VALIDATION_TYPE).asText());
							errorObject.put(Constants.FIELD, depedentColumn.asText());
						
							errorObject.put(Constants.MESSAGE_KEY,
									in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_057));
							((ObjectNode) paramNode).put("baseQuestionLabel", columnNode.get(Constants.CODE).asText());
							((ObjectNode) paramNode).put(Constants.DISPLAYLABEL,
									JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
											: columnNode.get(Constants.DISPLAYLABEL).asText());
							errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
							errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.ELIGIBILITY, null,
									null, null, null);
							arr.add(errorObject);
						}
						// MDA ACTION
						performMDAAction = true;

					}
				}
				if (performMDAAction && isMDARequest) {
					((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), Constants.ANSWER_YES);
				}

			} else {
				boolean someAns = false;
				for (JsonNode depedentColumn : depedentColumns) {
					if (!(JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
							|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), Constants.NULL)
									|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), "")))) {
						someAns=true;
						break;
					}
				}
				if(someAns)
				{
					((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), Constants.ANSWER_YES);
					for (JsonNode depedentColumn : depedentColumns)
					{
						if ((JsonUtils.isNullOrBlankOrNullNode(dataNode.get(depedentColumn.asText()))
								|| (StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), Constants.NULL)
										|| StringUtils.equals(dataNode.get(depedentColumn.asText()).asText(), ""))) 
								&& metaDataColumnsOnly.contains(depedentColumn.asText()))
					{
					ObjectNode errorObject = mapper.createObjectNode();
					errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
					errorObject.put(Constants.VALIDATION_TYPE, validationOBJ.get(Constants.VALIDATION_TYPE).asText());
					errorObject.put(Constants.FIELD, depedentColumn.asText());
					errorObject.put(Constants.MESSAGE_KEY,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_058));
					((ObjectNode) paramNode).put(Constants.DISPLAYLABEL,
							JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
									: columnNode.get(Constants.DISPLAYLABEL).asText());
					errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
					errorObject = prepareOutputJson(errorObject, null, null, ValidationTypes.ELIGIBILITY, null, null,
							null, null);
					arr.add(errorObject);
					}
				}
				}
				else
				{
					ObjectNode errorObject = mapper.createObjectNode();
					errorObject.put(Constants.ERROR_TYPE, Constants.ALERT);
					errorObject.put(Constants.VALIDATION_TYPE, validationOBJ.get(Constants.VALIDATION_TYPE).asText());
					errorObject.put(Constants.FIELD, columnNode.get(Constants.CODE).asText());
					errorObject.put(Constants.MESSAGE_KEY,
							in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_050));
					((ObjectNode) paramNode).put("code", columnNode.get(Constants.CODE).asText());
					((ObjectNode) paramNode).put(Constants.DISPLAYLABEL,
							JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DISPLAYLABEL)) ? ""
									: columnNode.get(Constants.DISPLAYLABEL).asText());
					errorObject.set(Constants.MESSAGE_PARAMS, paramNode);
					arr.add(errorObject);
				}

				if (!someAns && isMDARequest) {
					((ObjectNode) (dataNode)).put(columnNode.get(Constants.CODE).asText(), Constants.ANSWER_NO);
				}
			}
		}
		return arr;
	}

	public List<ObjectNode> prepareRefernceRangeObject(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode) throws SQLException, IOException, CustomStatusException  {
		StringBuilder query = new StringBuilder();
		// ColumnMap
		Map<String, String> colMap = null;
		Map<String, String> rangeMap = null;
		String paramValue = "";
		JsonNode code = null;
		JsonNode mappedColName = null;
		HashMap<String, String> paramValueMap = null;
		String tableName = null;
		code = columnNode.get(Constants.CODE);
		mappedColName = columnNode.get(Constants.MAPPED_COLUMN_NAME);
		String metaDatatableName = null;
		ArrayList<ObjectNode> objList = new ArrayList<>();
		List<Object> list = null;
		ObjectNode objNode;
		
		Pattern tabPattern = Pattern.compile(Constants.TAB_PATTERN_RANGE);
		// ******************Start Cache Logic For Testing then Development
		// Purpose******************************** PE-8609(500 Error)

		CacheManager cm = CacheManager.getInstance();
		net.sf.ehcache.Cache cache = cm.getCache("cacheStore");
		Object cacheObj = cache.get("allTableList").getObjectValue();
		if (null != cacheObj) {
			list = Arrays.asList(cacheObj);
		}
		
		 ArrayList<Object> tableObjList = (ArrayList)list.get(0);
		 Map<String,String> tableMap = new HashMap<>();
		if (null != cacheObj) {
			for (Object obj : tableObjList) {
				tableMap.put(obj.toString(), obj.toString());
			}
		}

		// ******************End Cache Logic For Testing then Development
		// Purpose********************************
		metaDatatableName = validationOBJ.get(Constants.REFERENCE_TABLE_NAME).asText();

		if (!StringUtils.isEmpty(metaDatatableName)) {
			tableName = metaDatatableName;
		} else {
			throw new CustomStatusException(Constants.HTTPSTATUS_400,
					in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_051));
		}
		// PE : 9237 Reference table not exist in DB
		Matcher matcher = tabPattern.matcher(metaDatatableName);
		
		String cacheTableName = null;
		boolean isTableInCache = false;
		
		// PE-9504 Validation should be skipped if input data is missing for any column
		// name from the table.
		boolean isSkipped = false;

		if(!tableMap.isEmpty()) {
			cacheTableName = tableMap.get(metaDatatableName.toUpperCase());
			if(!StringUtils.isEmpty(cacheTableName)) {
				isTableInCache = true;
			}
		}
		
		if (!matcher.find() || !isTableInCache) {
			objNode = prepareOutputJson(validationOBJ, columnNode,
					in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_051),
					ValidationTypes.RANGEVALIDATIONREFTABLE, columnNode.get(Constants.CODE).asText(), "ERROR", null,
					null);
			// Logging error for ELK Stack to separate error file expression_error.log
			isSkipped=true;
			if( logger.isWarnEnabled() ){
				logger.warn(objNode.toString());
			}
		} 
		else {
			if (code != null && columnNode.get(Constants.DIMENSIONS) != null) {
				paramValueMap = prepareParamValuesMap(code, columnNode.get(Constants.DIMENSIONS), dataMap);
			} else if (code != null) {
				paramValueMap = new HashMap<>();
				paramValue = getParamValuesRange(code, dataMap, mappedColName);
				paramValueMap.put(Constants.BLANK, paramValue);
			}
			query.append(" SELECT ERROR_MIN,ERROR_MAX,ALERT_MIN,ALERT_MAX FROM " + tableName + " WHERE ");
			// colMap
			colMap = EBDbUtils.getTableSchemaData(tableName);
            colMap.remove(Constants.ALERT_MAX);
            colMap.remove(Constants.ALERT_MIN);
            colMap.remove(Constants.ERROR_MAX);
            colMap.remove(Constants.ERROR_MIN);

			// PE-7689 Start
			int j = 0;
			for (Map.Entry<String, String> entry : colMap.entrySet()) {
				if (j == 0) {
					if (entry.getKey().equalsIgnoreCase("CTX_CTRY_CODE")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_COUNTRY_CODE))
								&& !(dataMap.get(Constants.COTEXT_COUNTRY_CODE)).asText().equals("")) {
							j++;
							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_COUNTRY_CODE)).asText() + "'");
							} else {
								query.append(
										entry.getKey() + "=" + (dataMap.get(Constants.COTEXT_COUNTRY_CODE)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}
					} else if (entry.getKey().equalsIgnoreCase("CTX_SUPER_SECTOR")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR))
								&& !(dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR)).asText().equals("")) {
							j++;
							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR)).asText() + "'");
							} else {
								query.append(entry.getKey() + "="
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}
					} else if (entry.getKey().equalsIgnoreCase("CTX_SECTOR")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR))
								&& !(dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR)).asText().equals("")) {
							j++;
							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR)).asText() + "'");
							} else {
								query.append(entry.getKey() + "="
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}
					} else if (entry.getKey().equalsIgnoreCase("CTX_SUBSECTOR")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR))
								&& !(dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR)).asText().equals("")) {
							j++;
							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR)).asText() + "'");
							} else {
								query.append(entry.getKey() + "="
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}
						// If there is any other column in table which is not empty or blank in  input data section
						 // then skip the validation
					} else if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(entry.getKey()))
							&& !dataMap.get(entry.getKey()).asText().equals("")) {
						j++;
						if (EBDbUtils.isStringDataType(entry.getValue())) {
							query.append(entry.getKey() + "='" + (dataMap.get(entry.getKey()).asText() + "'"));
						} else {
							query.append(entry.getKey() + "=" + (dataMap.get(entry.getKey()).asText()));
						}

					} else {
						isSkipped = true;
						break;
					}
				} else {

					if (entry.getKey().equalsIgnoreCase("CTX_CTRY_CODE")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_COUNTRY_CODE))
								&& !(dataMap.get(Constants.COTEXT_COUNTRY_CODE)).asText().equals("")) {
							j++;

							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_COUNTRY_CODE)).asText() + "'");
							} else {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "="
										+ (dataMap.get(Constants.COTEXT_COUNTRY_CODE)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}
					} else if (entry.getKey().equalsIgnoreCase("CTX_SUPER_SECTOR")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR))
								&& !(dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR)).asText().equals("")) {
							j++;
							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR)).asText() + "'");
							} else {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "="
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUPERSECTOR)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}
					} else if (entry.getKey().equalsIgnoreCase("CTX_SECTOR")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR))
								&& !(dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR)).asText().equals("")) {
							j++;

							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR)).asText() + "'");
							} else {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "="
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SECTOR)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}

					} else if (entry.getKey().equalsIgnoreCase("CTX_SUBSECTOR")) {
						if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR))
								&& !(dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR)).asText().equals("")) {
							j++;

							if (EBDbUtils.isStringDataType(entry.getValue())) {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "='"
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR)).asText() + "'");
							} else {
								query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "="
										+ (dataMap.get(Constants.COTEXT_INDUSTRY_SUBERSECTOR)).asText());
							}
						} else {
							isSkipped = true;
							break;
						}

					} else if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(entry.getKey()))
							&& !dataMap.get(entry.getKey()).asText().equals("")) {
						j++;
						if (EBDbUtils.isStringDataType(entry.getValue())) {
							query.append(
									Constants.SPACE_AND_SPACE + entry.getKey() + "='" + (dataMap.get(entry.getKey()).asText() + "'"));

						} else {
							query.append(Constants.SPACE_AND_SPACE + entry.getKey() + "=" + (dataMap.get(entry.getKey()).asText()));

						}
					} else {
						isSkipped = true;
						break;
					}
				}
			}
		}

		if (!isSkipped) {
			query.append(CustomFunctions.FIRST_ROW);

			// PE-7689 End
			rangeMap = DBUtils.getRangeDetails(query.toString());
			if (null != rangeMap) {
				if (null != rangeMap.get("SQLException")) {
					return Collections.emptyList();
				}
				objList = getRangeResult(paramValueMap, validationOBJ, columnNode, 
						rangeMap.get("ERROR_MIN"), rangeMap.get("ALERT_MIN"), rangeMap.get("ERROR_MAX"),
						rangeMap.get("ALERT_MAX"));
			}
		}
			return objList;
		}
		


	// Reference table range logic check PE : 6738, PE 6027 start
	public static String getParamValuesRange(JsonNode code, HashMap<String, JsonNode> dataMap, JsonNode mappedColName)
			{
		String paramValue = "";
		if (code != null && !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code.asText()))) {
			if (dataMap.get(code.asText()).asText() != null
					&& !dataMap.get(code.asText()).asText().equals(Constants.BLANK)) {
				paramValue = dataMap.get(code.asText()).asText();
			} else if (mappedColName != null && mappedColName.asText() != null
				&& dataMap.get(mappedColName.asText()).asText() != null
				&& !dataMap.get(mappedColName.asText()).asText().equals(Constants.BLANK)) {
					paramValue = dataMap.get(mappedColName.asText()).asText();
			}
		}
		return paramValue;
	}
	// Reference table range logic check PE : 6738

	public static ArrayList<ObjectNode> getRangeResult(HashMap<String, String> paramValueMap, JsonNode validationOBJ,
			JsonNode columnNode, String minError, String minAlert, String maxError, String maxAlert)
			{

		String warning = "";
		String warningMsg = null;
		String value = "";
		ObjectNode obj = null;
		ArrayList<ObjectNode> objList = new ArrayList<>();

		if (paramValueMap != null && paramValueMap.size() > 0) {
			for (Map.Entry<String, String> entry : paramValueMap.entrySet()) {
				warningMsg = null;
				warning="";
				value = entry.getValue();
				if (JsonUtils.isStringExist(value)) {
					if (DataUtility.isStringNotNullAndBlank(minError) && !StringUtils.isEmpty(value)
							&& Double.parseDouble(value) < Double.parseDouble(minError)) {
						warning = Constants.ERROR;
						//"minError"
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_024);
					}
					if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxError)
							&& Double.parseDouble(value) > Double.parseDouble(maxError)) {
						warning = Constants.ERROR;
						// "maxError"
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_027);
					}
					if (null == warningMsg && DataUtility.isStringNotNullAndBlank(minAlert)
							&& Double.parseDouble(value) < Double.parseDouble(minAlert)) {
						warning = Constants.ALERT;
						// "minAlert"
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_025);
					}
					if (null == warningMsg && DataUtility.isStringNotNullAndBlank(maxAlert)
							&& Double.parseDouble(value) > Double.parseDouble(maxAlert)) {
						warning = Constants.ALERT;
						// "maxAlert"
						warningMsg = in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_026);
					}
				}
				if (JsonUtils.isStringExist(warningMsg) && JsonUtils.isStringExist(warning)) {
					if (warning.equals(Constants.ERROR))
						obj = APIExpressionEvaluator.prepareOutputJson(validationOBJ, columnNode, warningMsg,
								ValidationTypes.RANGEVALIDATIONREFTABLE, columnNode.get(Constants.CODE).asText(),
								"ERROR", null, entry.getKey()); // PE-3711
					else if (warning.equals(Constants.ALERT))
						obj = APIExpressionEvaluator.prepareOutputJson(validationOBJ, columnNode, warningMsg,
								ValidationTypes.RANGEVALIDATIONREFTABLE, columnNode.get(Constants.CODE).asText(),
								"ALERT", null, entry.getKey());
					objList.add(obj);
				}

			}
		}
		return objList;
	}
	// Reference table range logic check PE : 6738, PE 6027 end

	public static HashMap<String, String> prepareParamValuesMap(JsonNode code, JsonNode dimension,
			Map<String, JsonNode> dataMap) {

		ArrayList<String> dimList = null;
		StringBuilder key = null;
		key = new StringBuilder();
		HashMap<String, String> paramValueMap = new HashMap<String, String>();
		dimList = extractDimensions(dimension);

		for (int i = 0; i < dimList.size(); i++) {
			key.delete(0, key.length());
			key.append(code.asText()).append(".").append(dimList.get(i));

			if (dataMap.get(key.toString()) != null) {
				paramValueMap.put(dimList.get(i), dataMap.get(key.toString()).asText());
			}
		}
		return paramValueMap;

	}

	public static ArrayList<String> extractDimensions(JsonNode node)  {
		ArrayList<String> list = new ArrayList<String>();
		if (node != null && node.getNodeType() == JsonNodeType.ARRAY) {
			Iterator<JsonNode> it = node.iterator();
			while (it.hasNext()) {
				list.add(it.next().asText());
			}
		}
		return list;
	}

	// PE7044 This method will execute the MDA Set Default expression and return
	// the result of the expression
	public String expressionEvaluatorForMDA(HashMap<String, JsonNode> dataMap, JsonNode validationOBJ,
			JsonNode columnNode, Map<String, Integer> cscDuplicateChkMap, List<String> metaDataColumns) throws CustomStatusException {
		String expr = "";
		String result = "";
		boolean checkBox = false;
		String fieldArr[] = null;
		String prefix = Constants.THIS;
		String patternString = "";
		ArrayList<String> paramValueList = null;
		Pattern ext_pattern = Pattern.compile(Constants.EXP_PATTERN);
		ObjectNode obj = null;
		boolean isHiddenQue=false;
		// Start PE- 7056
		ArrayList<String>  metaDataColumnsOnly= (ArrayList<String>) metaDataColumns;
		boolean isExpressionFlag = true;
		boolean thisAndContextFlag = false; 
		// End PE- 7056
		
		if (columnNode != null && columnNode.get(Constants.QUESTION_TYPE) != null
				&& columnNode.get(Constants.QUESTION_TYPE).asText().equalsIgnoreCase("checkboxes")) {
			checkBox = true;
		}
		if (!JsonUtils.isNullOrBlankOrNullNode(columnNode) && !JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.HIDDEN)) 
				&& columnNode.get(Constants.HIDDEN).asText().equalsIgnoreCase("true")) {
			isHiddenQue = true;
		} 
		Map<String, Object> paramValues = new HashMap<>();
		if (validationOBJ != null && validationOBJ.get(Constants.MDA) != null) {
			expr = validationOBJ.get(Constants.MDA).asText().replaceAll(Constants.CURLY_BRACES, "");
			patternString = expr;
			if (expr != null && expr.contains("bool_in_lookup")) {
				patternString = expr.replaceAll(Constants.REPLACE_FROM, Constants.REPLACE_TO);
			}
			Matcher matcher = ext_pattern.matcher(patternString);
			List<String> fields = new ArrayList<String>();
			while (matcher.find()) {
				fields.add(matcher.group().replace(Constants.THIS, "").trim());
			}
			
			 // PE - 7056	
			if( !fields.isEmpty() ) {  	
				thisAndContextFlag = true;
			}
			if ( thisAndContextFlag ) {
					if ( (!metaDataColumnsOnly.isEmpty() &&  chekElementExist(metaDataColumnsOnly,fields)) || isHiddenQue) { 
						isExpressionFlag = true;
					}else{
						isExpressionFlag = false;
					}
			 }
			if ( isExpressionFlag ) {   // PE - 7056
				String fieldtrim = "";
			for (String field : fields) {
				if (null != field) {
					fieldtrim = field.trim();
					if (fieldtrim.contains(Constants.CONTEXTDATAKEY)) {
						prefix = prefix.replace(Constants.THIS, "");
					}
				}
				if (checkBox) {
					fieldArr = extractFields(fieldtrim);
					
					Object otherSectionData = null;
					if (fieldArr != null) {
						
						if(dataMap.get(fieldArr[0]) == null){
							otherSectionData = dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldArr[0].trim()) == null ? Constants.NULL
									: findInArray(dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldArr[0]), fieldArr[1]);
						}else{
							otherSectionData = findInArray(dataMap.get(fieldArr[0]), fieldArr[1]);
						}
						
						paramValues.put(prefix + fieldtrim, otherSectionData);
						
					} else {
						
						if(dataMap.get(fieldtrim) == null){
							otherSectionData = (dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldtrim) == null ? Constants.NULL
									: getContextDataValues(dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldtrim).asText().replace(",", "")));
						}else{
							otherSectionData = getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(fieldtrim, Constants.DATA_TYPE),
									dataMap.get(fieldtrim).asText().replace(",", ""));
						}
						
						paramValues.put(prefix + fieldtrim,otherSectionData);
					}
				} else {
					String tmp = null;
					if (dataMap.get(fieldtrim) == null && !isHiddenQue) {
						if (dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldtrim) == null) {
							if (cscDuplicateChkMap != null && cscDuplicateChkMap.containsKey(fieldtrim)) {
								paramValueList = new ArrayList<String>();
								paramValueList.add("" + cscDuplicateChkMap.get(fieldtrim));
								paramValueList.add("" + fieldtrim);
								paramValueList.add("" + expr);
								throw new CustomStatusException(Constants.HTTPSTATUS_400,
										in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_048),
										in.lnt.utility.general.Cache.getPropertyFromError(
												ErrorCacheConstant.ERR_048 + Constants.UNDERSCORE_PARAMS),
										paramValueList);// Cannot find variable
							} else {
								
								throw new JSONException(String.format(Constants.ERROR_CANNOT_FIND_VARIABLE_IN_EXPR,
										fieldtrim, expr));
							}
						} else {
							tmp = getContextDataValues(
									dataMap.get(Constants.OTHER_SECTIONSDATA_PERIOD + fieldtrim).asText().replace(",", ""));
						}
					} else {
						tmp = getDataTypeBasedValue(columnDataTypeMapping.getOrDefault(fieldtrim, Constants.DATA_TYPE),
								dataMap.get(fieldtrim).asText().replace(",", ""));
					}
					paramValues.put(prefix + fieldtrim, tmp);
				}
				prefix = Constants.THIS;
			}
			expr = expr.replace(Constants.NULL, "\'null\'");
			try {
				result = expressionEvalutionDriver.expressionEvaluatorWithObject(paramValues, expr, false);

			} catch (ExpressionEvaluatorException e) {
				logger.warn("Erroneous expression is: "+ expr +", Reason " +e.getMessage() );
				return null;
			}catch (Exception e) {
				logger.error("Excepion occured in expressionEvaluator.." + e.getMessage());
				paramValueList = new ArrayList<String>();
				paramValueList.add(e.getMessage());
				result = Constants.INVALID_EXP;//PE-8032
			  }
		   }else {//PE-8032
				result = Constants.INVALID_EXP;
			}
		}
		return result;
	}
	
	/**
	 * @author Akhileshwar
	 * @throws Exception
	 * PE-7045 List of validation objects for the node on the basis of sequential validation
	 */
	
	public List<ObjectNode> performSequentialCheck(Map<String, JsonNode> dataMap, JsonNode validationNode,
			JsonNode node, JsonNode dataNode, Map<String, JsonNode> dimMap, Map<String, String> columnDataTypeMap,
			boolean uploadMultipleFile, List<String> metaDataColumnsOnly)
			 {
		ObjectNode obj = null;
		List<ObjectNode> objList = new ArrayList<>();
		String dataType = "";
		String currentColumn = "";
		String nextColumn = "";
		JsonNode dimNode = null;
		List<String> dimList;
		List<String> nextDimList;
		String result = "";

		boolean flag = true;
		boolean nodeDimension = false;
		boolean nextNodeDimension = false;
		String dimensionExtension = "";
		Map<String, List<String>> colDimMap = new HashMap<String, List<String>>();
		StringBuilder colBuffer = new StringBuilder();
		Pattern INT_PATTERN = Pattern.compile(Constants.INT_PATTERN);
		Pattern DOUBLE_PATTERN = Pattern.compile(Constants.DOUBLE_PATTERN);
		Pattern LONG_PATTERN = Pattern.compile(Constants.LONG_PATTERN);

		String currentVal = "";
		String nextVal = "";
		JsonNode valuesNode = validationNode.get("dependentColumns");
		ArrayList<String> columnList = new ArrayList<>();
		ArrayList<String> columnListTemp = new ArrayList<>();

		for (int i = 0; i < valuesNode.size(); i++) {
		 if(metaDataColumnsOnly.contains(valuesNode.get(i).asText())){//PE 7989 if condition added
			dataType = columnDataTypeMap.get(valuesNode.get(i).asText());
			if (dataType.equalsIgnoreCase(Constants.INT) || dataType.equalsIgnoreCase(Constants.INTEGER)
					|| dataType.equalsIgnoreCase(Constants.BIGINT) || dataType.equalsIgnoreCase(Constants.FLOAT)
					|| dataType.equalsIgnoreCase(Constants.DOUBLE)) {

				columnListTemp.add(valuesNode.get(i).asText());
			}
			}
		}
		if (columnListTemp.size() > 1) {
			for (String col : columnListTemp) {
				flag = false;
				dimNode = dimMap.get(col);
				if (dimNode != null) {
					dimList = APIExpressionEvaluator.extractDimensions(dimNode);
					for (String dimension : dimList) {
						colBuffer.delete(0, colBuffer.length());
						colBuffer.append(col).append(".").append(dimension);
						if (dataMap != null && !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(colBuffer.toString()))
								&& !dataMap.get(colBuffer.toString()).asText().isEmpty()) {
							flag = true;
							colDimMap.put(col, dimList);
							break;
						}
					}

				} else {
					if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(col)) && !dataMap.get(col).asText().isEmpty()
							&& !Constants.NULL.equalsIgnoreCase(dataMap.get(col).asText().trim())) {
						flag = true;
					}
				}
				if (flag) {
					columnList.add(col);
				}
			}
		}

		if (columnList.size() > 1) {
			for (int i = 0; i < columnList.size() - 1; i++) {
				String thisColumnDefault = columnList.get(i);
				String nextColumnDefault = columnList.get(i + 1);

				nodeDimension = false;
				nextNodeDimension = false;
				dimList = colDimMap.get(thisColumnDefault);
				nextDimList = colDimMap.get(nextColumnDefault);

				if (dimList != null && nextDimList != null) {
					dimList.retainAll(nextDimList);
					nodeDimension = true;
					nextNodeDimension = true;
				} else if (dimList == null && nextDimList != null) {
					dimList = nextDimList;
					nextNodeDimension = true;
				} else if (dimList != null && nextDimList == null) {
					nodeDimension = true;
				}

				currentColumn = thisColumnDefault;
				nextColumn = nextColumnDefault;

				if (nodeDimension == true || nextNodeDimension == true) {
					if (dimList != null) {
						for (String dim : dimList) {
							dimensionExtension = "." + dim;
							currentColumn = thisColumnDefault;
							nextColumn = nextColumnDefault;
							if (nodeDimension) {
								currentColumn += dimensionExtension;
							}

							if (nextNodeDimension) {
								nextColumn += dimensionExtension;
							}
							if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(currentColumn))) { // MAY
																									// FAIL
								currentVal = dataMap.get(currentColumn).asText();
							} else {
								currentVal = null;
							}

							if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(nextColumn))) { // MAY
																								// FAIL
								nextVal = dataMap.get(nextColumn).asText();
							} else {
								nextVal = null;
							}

							if ((currentVal != null && !currentVal.isEmpty() && nextVal != null && !nextVal.isEmpty()) 
								&& (((INT_PATTERN.matcher(currentVal).matches())
										|| (DOUBLE_PATTERN.matcher(currentVal).matches())
										|| (LONG_PATTERN.matcher(currentVal).matches()))
										&& ((INT_PATTERN.matcher(nextVal).matches())
										|| (DOUBLE_PATTERN.matcher(nextVal).matches())
										|| (LONG_PATTERN.matcher(nextVal).matches()))) 
									&& (dataMap.get(currentColumn).asDouble() >= dataMap.get(nextColumn).asDouble())) {
										currentColumn = thisColumnDefault;
										nextColumn = nextColumnDefault;
										dimNode = mapper.createObjectNode();
										for (String dim1 : dimList) {
											if (dim1.equals(dim) && !uploadMultipleFile) {
												((ObjectNode) dimNode).put(dim1, "");
											} else {
												String val = null;
												if (!JsonUtils.isNullOrBlankOrNullNode(
														dataMap.get(currentColumn + "." + dim1))) {
													val = dataMap.get(currentColumn + "." + dim1).asText();
												}
												((ObjectNode) dimNode).put(dim1, val);
											}
										}
										((ObjectNode) (dataNode)).set(currentColumn, dimNode);

										dimNode = mapper.createObjectNode();
										for (String dim1 : dimList) {
											if (dim1.equals(dim) && !uploadMultipleFile) {
												((ObjectNode) dimNode).put(dim1, "");
											} else {
												// CHANGE HERE
												if (!JsonUtils.isNullOrBlankOrNullNode(
														dataMap.get(nextColumn + "." + dim1))) {
													((ObjectNode) dimNode).put(dim1,
															dataMap.get(nextColumn + "." + dim1).asText());
												}
											}
										}
										((ObjectNode) (dataNode)).set(nextColumn, dimNode);
										obj = prepareOutputJson(validationNode, node, result,
												ValidationTypes.SEQUENTIALCHECK, nextColumn, currentColumn, null, dim);
										objList.add(obj);
							}
						}
					}
				} else {
					if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(currentColumn))) {
						currentVal = dataMap.get(currentColumn).asText(); // HANDLE
																			// THIS
					} else {
						currentVal = null;
					}
					if (!JsonUtils.isNullOrBlankOrNullNode(dataMap.get(nextColumn))) {
						nextVal = dataMap.get(nextColumn).asText(); // HANDLE
																	// THIS
					}

					if ((currentVal != null && !currentVal.isEmpty() && nextVal != null && !nextVal.isEmpty()) 
						&& (((INT_PATTERN.matcher(currentVal).matches())
						|| (DOUBLE_PATTERN.matcher(currentVal).matches())
						|| (LONG_PATTERN.matcher(currentVal).matches()))
						&& ((INT_PATTERN.matcher(nextVal).matches())
						|| (DOUBLE_PATTERN.matcher(nextVal).matches())
						|| (LONG_PATTERN.matcher(nextVal).matches()))) 
						&& (dataMap.get(currentColumn).asDouble() >= dataMap.get(nextColumn).asDouble())) {
								currentColumn = thisColumnDefault;
								nextColumn = nextColumnDefault;
								if(!uploadMultipleFile){
									((ObjectNode) (dataNode)).put(currentColumn, "");
									((ObjectNode) (dataNode)).put(nextColumn, "");
								}
								obj = prepareOutputJson(validationNode, node, result, ValidationTypes.SEQUENTIALCHECK,
										nextColumn, currentColumn, null, "");
								objList.add(obj);
					}

				}
			}
		}
		return objList;
	}
	
	
	public static boolean chekElementExist(List<String> superSet, List<String> subset) {
		
		return superSet.containsAll(subset);
	}
	
	/**
	 * PE-7241
	 * @param inputValue
	 * @param key
	 * @param dataType
	 * @return
	 * @throws Exception
	 */
	public ObjectNode checkRequiredField(String inputValue, String key, String dataType) {
		ObjectNode obj = mapper.createObjectNode();
		if (StringUtils.isBlank(inputValue)) {
			return prepareOutputJson(null, null, null, ValidationTypes.REQUIRED, key, inputValue, dataType, null);
		}
		return obj;
	}
	
	/**
	 * 
	 * @param field
	 * @param validationType
	 * @param originalvalue
	 * @param dataType
	 * @return
	 */
	public static ObjectNode prepareMoneyAutocorrectObj(String field, String originalvalue, String dataType, String questionType)
	{
		ObjectNode validation = mapper.createObjectNode();
		validation.put(Constants.FIELD, field);
		validation.put(Constants.ERROR_TYPE, Constants.AUTOCORRECT);
		if(questionType.equals(Constants.MONEY)) {
		validation.put(Constants.MESSAGE_KEY,
				in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_066));
		}/*else if(questionType.equals(Constants.YEAR)) {
			validation.put(Constants.MESSAGE_KEY,
					in.lnt.utility.general.Cache.getPropertyFromError(ErrorCacheConstant.ERR_067));	
		}*/
		validation.put(Constants.ORIGINAL_VALUE,originalvalue);
		validation.put(Constants.DATA_TYPE, dataType);
		validation.put(Constants.QUESTION_TYPE, questionType);
		return validation;
	}	
}