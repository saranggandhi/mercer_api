package in.lnt.validations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.parser.driver.ExpressionEvalutionDriver;

import in.lnt.constants.Constants;
import in.lnt.enums.ValidationTypes;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.parser.CSVParser;
import in.lnt.service.db.DBUtils;
import in.lnt.utility.general.DataUtility;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.evaluator.APIExpressionEvaluator;

public class FormValidator_CSC {
	private static final Logger logger = LoggerFactory.getLogger(FormValidator_CSC.class);
	ObjectMapper mapper = new ObjectMapper();
	List<String> uuidList = new ArrayList<>();
	static Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^(.+)@(.+)$", Pattern.CASE_INSENSITIVE);
	static Pattern VALID_PHONE_NUMBER_REGEX = 
		    Pattern.compile("^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$", Pattern.CASE_INSENSITIVE);
	// ^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$
	
	public JsonNode parseAndValidate_CrossSectionCheck(String entitiesJson, boolean isMDARequest) throws Exception {
		JsonNode resultNode = null;
		ArrayNode entityArr = null;
		ArrayNode arr = null;
		JsonNode resultEntiryNode=null;
		HashMap<String,JsonNode> dimMap = new HashMap<>();
		JsonNode dataNode=null;
		JsonNode contextDataNode=null;
		JsonNode entityNode=null; 
		entityArr= mapper.createArrayNode();
		
		Map.Entry<String, JsonNode> entry = null;
		Map.Entry<String, JsonNode> entry1 = null;
		ArrayNode entitiesNode = mapper.readValue(entitiesJson, ArrayNode.class);				// "Entities" node
		JsonNode sectionsNode = null;															// "sections"
		
		// PE-7056 contexDataKeys and otherDataKeys.are append together in otherAndContextKeys.
		List<String> otherAndContextKeys = new ArrayList<>();
		
		HashMap<String, JsonNode> dataMap = new HashMap<>();
		Iterator<Map.Entry<String, JsonNode>> it =null;
		Iterator<Map.Entry<String, JsonNode>> contextIt = null;
		
		// Fixed along with PE-8070
		List<ArrayList<JsonNode>> columnNodeList_Master = null;
		List<ArrayList<HashMap<String, JsonNode>>> dataMap2_Sections = null;
		Set<String> metaDataColumnsOnly;
		APIExpressionEvaluator.df.setMaximumFractionDigits(16);
		APIExpressionEvaluator.df.setMinimumFractionDigits(1);

			
			/************ Iterating entities *****************/
			for(int i=0;i<entitiesNode.size();i++) {
				entityNode = entitiesNode.get(i);
			
				resultEntiryNode = mapper.createObjectNode(); 
				sectionsNode = entityNode.get(Constants.SECTIONS);
				contextDataNode = entityNode.get(Constants.CONTEXTDATAKEY);
				metaDataColumnsOnly = new HashSet<>();

				/*
				 *  1. Populate "dataMap" with "contextData" object
				 */
				if (contextDataNode != null && !contextDataNode.toString().equals("")) {
					contextIt = contextDataNode.fields();
					while (contextIt.hasNext()) {
						entry = contextIt.next();
						JsonNode value = JsonUtils.isNullNode(entry.getValue())?null:entry.getValue();
						if (entry.getValue().getNodeType() != JsonNodeType.OBJECT						// PE-8070
								&& entry.getValue().getNodeType() != JsonNodeType.ARRAY) {
							dataMap.put(Constants.CONTEXTDATAKEY + "." + entry.getKey().trim(), value);
							//PE-7056
							otherAndContextKeys.add(Constants.CONTEXTDATAKEY + "." + entry.getKey().trim());
						} else if (null != value && value.getNodeType() == JsonNodeType.ARRAY) {
							Iterator<Map.Entry<String, JsonNode>> arrIt = value.get(0).fields();
							while (arrIt.hasNext()) {
								entry1 = arrIt.next();
								dataMap.put(Constants.CONTEXTDATAKEY + ".industry." + entry1.getKey().trim(),
										entry1.getValue());
								//PE-7056
								otherAndContextKeys.add(Constants.CONTEXTDATAKEY + ".industry." + entry1.getKey().trim());
							}
						} else {
							dataMap = new FormValidator().parseJsonObjectMap(entry.getKey(), value, dataMap,
									Constants.CONTEXTDATAKEY, null,otherAndContextKeys);
						}
					}
				}
				/*
				 *  1. Ended
				 */

				/*********** 2. Iterating sections ******************/
				/* Generate : 
				 * dataMap2_Sections => [[{Q1=a,Q2=b},{Q1=x,Q2=y}],[{Q6=q},{Q6=r},{Q6=s}]]
				 */
				columnNodeList_Master = new ArrayList<>();
				dataMap2_Sections = new ArrayList<>();
				
				//Creating a ExpressionEvalutionDriver at a request level(Per Entity (Eg. Employee section))
				ExpressionEvalutionDriver expressionEvalutionDriver = new ExpressionEvalutionDriver();
				
				APIExpressionEvaluator apiExpressionEvaluator = new APIExpressionEvaluator(expressionEvalutionDriver);
				
				logger.debug("parseAndValidate_CrossSectionCheck >> ExpressionEvaluatorDriver Cache : {}",
						apiExpressionEvaluator.getExpressionEvaluatorDriver().getAggregateFunctionCache());

				
				//Set Column - data type mapping - PE
				apiExpressionEvaluator.setColumnDataTypeMapping(new HashMap<String,String>());
				
				for (int sctnCtr=0; sctnCtr<sectionsNode.size(); sctnCtr++){
					ArrayList<HashMap<String, JsonNode>> dataMap2 = new ArrayList<>();
					
					/******** 2.1 Iterating columns : Generate columnNodeList_Master for all sections ********/
					ArrayList<JsonNode> columnNodeList_Section = new ArrayList<>();
					JsonNode sectionNode_sectionStructure_columns = sectionsNode.get(sctnCtr).get(Constants.SECTION_STRUCTURE).get(Constants.COLUMNS);
					for(int clmCtr = 0; clmCtr<sectionNode_sectionStructure_columns.size(); clmCtr++){
						columnNodeList_Section.add(sectionNode_sectionStructure_columns.get(clmCtr));

						// SG : Start PE-7056 Expression should not executed if columns are not present in MetaData.
						// Populate "metaDataColumnsOnly"
						if(!JsonUtils.isNullOrBlankOrNullNode(
								sectionNode_sectionStructure_columns.get(clmCtr).get(Constants.CODE))) {
							metaDataColumnsOnly.add(
								sectionNode_sectionStructure_columns.get(clmCtr).get(Constants.CODE).asText());
							
							// PE-8070 : Add dimensions also in "metaDataColumnsOnly"
							if(sectionNode_sectionStructure_columns.get(clmCtr).has(Constants.DIMENSIONS)) {
								
								ArrayNode dimensionArray = (ArrayNode)sectionNode_sectionStructure_columns.get(clmCtr).get("dimensions");
								for(int j = 0; j<dimensionArray.size(); j++) {
									if(!JsonUtils.isNullOrBlankOrNullNode(dimensionArray.get(j))) {
										metaDataColumnsOnly.add(sectionNode_sectionStructure_columns.get(clmCtr).get(Constants.CODE).asText()
											+"."+dimensionArray.get(j).asText());
									}
								}
							}
						}
						// End PE-7056 
						
					}
					columnNodeList_Master.add(columnNodeList_Section);
					/******** 2.1 Ended ********/
					
					for (ArrayList<JsonNode> columnNodeList : columnNodeList_Master) {
						for (JsonNode columnNode : columnNodeList) {							
							if ((null != columnNode.get(Constants.CODE)
									&& !StringUtils.isBlank(columnNode.get(Constants.CODE).asText()))
									&& (null != columnNode.get(Constants.DATA_TYPE))) {
								apiExpressionEvaluator.getColumnDataTypeMapping().put(
										columnNode.get(Constants.CODE).asText(),
										columnNode.get(Constants.DATA_TYPE).asText());
								/**
								 * @author Akhileshwar
								 * PE-7045
								 */
								// dimension map created
								if (columnNode.get(Constants.DIMENSIONS) != null) {
									for (JsonNode dimensioNode : columnNode.get(Constants.DIMENSIONS)) {	// Code by Vivek
										apiExpressionEvaluator.getColumnDataTypeMapping().put(columnNode.get(Constants.CODE).asText() +Constants.DOT + dimensioNode.asText(),
												columnNode.get(Constants.DATA_TYPE).asText());
									}
									dimMap.put(columnNode.get(Constants.CODE).asText(),columnNode.get(Constants.DIMENSIONS));
								}
								
							}
						}
					}
					
					/*********** Iterating Data : Generate DataMap for all sections ******************/
					dataNode = mapper.createArrayNode();
					for(int dataCtr = 0; dataCtr<sectionsNode.get(sctnCtr).get(Constants.DATA).size(); dataCtr++){
						HashMap<String, JsonNode> dataMapTmp = new HashMap<>();
						
						// PE-7506
						
						JsonNode dNode = sectionsNode.get(sctnCtr).get(Constants.DATA).get(dataCtr);
						JsonNode sect_sectStruc_col = sectionsNode.get(sctnCtr).get(Constants.SECTION_STRUCTURE).get(Constants.COLUMNS);
							
							it = dNode.fields();
							while (it.hasNext()) {
								entry = it.next();
								JsonNode value = JsonUtils.isNullNode(entry.getValue()) ? null : entry.getValue();
								if(null != value) {
									if (value.getNodeType() != JsonNodeType.OBJECT) {
										dataMapTmp.put(entry.getKey().trim(), value);
									} else {
										dataMapTmp = new FormValidator().parseJsonObjectMap(entry.getKey(), value, dataMapTmp,
												null, sect_sectStruc_col,null);				// PE-7506
									}
								}else{
									dataMapTmp.put(entry.getKey().trim(), null);
								}
							}
							((ArrayNode)dataNode).add(dNode);
						for (JsonNode columnNode : sect_sectStruc_col) {
							if (!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.DIMENSIONS))) {
								dimMap.put(columnNode.get(Constants.CODE).asText(), columnNode.get(Constants.DIMENSIONS));
							}
							
							// PE-8292 Add null to dataMapTmp if Question is present in metadata-json but absent in data-json
							if (!JsonUtils.isNullOrBlankOrNullNode(columnNode.get(Constants.CODE))
									&& !dataMapTmp.containsKey(columnNode.get(Constants.CODE).asText())) {
								dataMapTmp.put(columnNode.get(Constants.CODE).asText().trim(), null);
							}
						}
						dataMapTmp= new FormValidator().updateDataMapWithEmptyDimension(dataMapTmp,dimMap);
						dataMap2.add(dataMapTmp);
						
					}
					//Making csv out of JSON single level
					if(null != dataNode) {

						JSONArray myArr = new JSONArray(dataNode.toString());
						Set<String> keyList = new HashSet();
						for (int j = 0; j < myArr.length(); j++) {
							JSONObject json = myArr.getJSONObject(j);
							Iterator<String> keys = json.keys();
							while (keys.hasNext()) {
								keyList.add(keys.next());
							}
						}
						CSVParser.parseFile(new ByteArrayInputStream(
								(JsonUtils.rowToString(new JSONArray(keyList)) + JsonUtils.toString(new JSONArray(keyList), myArr))
										.toString().getBytes("UTF-8")));
					
					}
					uuidList.add(apiExpressionEvaluator.getExpressionEvaluatorDriver().setCsvData(CSVParser.dataForAggregation, null));
					
					dataMap2_Sections.add(dataMap2);
					
				}
				/*********** 2. Ended ******************/
			
//				//Start PE-7056 Expression should not executed if columns are not present in MetaData.
				if(!otherAndContextKeys.isEmpty()){
					metaDataColumnsOnly.addAll(otherAndContextKeys);
				}
//				//End PE-7056  
				
				/*********** 3. Iterating sections and perform validations ***********/
				JsonNode sectionNodeNum = null;
				ArrayNode sectionArray = mapper.createArrayNode();
				for (int sctnCtr=0; sctnCtr<sectionsNode.size(); sctnCtr++){
					sectionNodeNum = mapper.createObjectNode();
					arr = mapper.createArrayNode();

					/*********** 3.1 Generate DataMap for the section (not including self dataMap values) ***********/
					int c=0;
					Map<String, Integer> duplicateChkMap = new HashMap<>();
					for(ArrayList<HashMap<String, JsonNode>> dataMap2: dataMap2_Sections){
						if(sctnCtr != c){
							for(HashMap<String, JsonNode> hm : dataMap2 ){
								for(Map.Entry<String,JsonNode> e : hm.entrySet()){
									dataMap.put(e.getKey(), e.getValue());			// Put key in dataMap
									if(duplicateChkMap.containsKey(e.getKey())){
										int val= Integer.parseInt(duplicateChkMap.get(e.getKey()).toString());
										duplicateChkMap.put(e.getKey(), val+1);
									}else{
										duplicateChkMap.put(e.getKey(), 1);
									}
								}
							}
						}
							
						c++;
					}
					
					for(Map.Entry<String, Integer> e : duplicateChkMap.entrySet()){
						if(e.getValue()!=1){
							dataMap.remove(e.getKey());
						}
					}
					/*********** 3.1 Ended ******************/

					/*********** 3.2 Iterating Rows (populating self dataMap values) and perform validations ***********/
					for(int dataCtr = 0; dataCtr<sectionsNode.get(sctnCtr).get(Constants.DATA).size(); dataCtr++){
						
						HashMap<String, JsonNode> dataMapFinal = new HashMap<>();
						
						// Put all other section dataMap
						dataMapFinal.putAll(dataMap);
						
						// Put all self section dataMap
						dataMapFinal.putAll(dataMap2_Sections.get(sctnCtr).get(dataCtr));
						
						dataNode = sectionsNode.get(sctnCtr).get(Constants.DATA);
						//7045 dimMap added  and 7056 metaDataColumnsOnly
						resultNode = validate(dataMapFinal, columnNodeList_Master.get(sctnCtr), dataNode.get(dataCtr), 
								apiExpressionEvaluator, duplicateChkMap,isMDARequest,dimMap, new ArrayList(metaDataColumnsOnly));
						arr.add(resultNode);
						
						dataMapFinal.clear();
					}		// SECTION END
					/*********** 3.2 Ended ***********/
					
					((ObjectNode) (sectionNodeNum)).set(Constants.SECTIONCODE, sectionsNode.get(sctnCtr).get(Constants.SECTIONCODE));
					((ObjectNode) (sectionNodeNum)).set(Constants.DATA, arr);
					((ArrayNode) sectionArray).add(sectionNodeNum);
					
				}
				/*********** 3. Ended ***********/

				((ObjectNode) resultEntiryNode).set(Constants.SECTIONS, sectionArray);					// Add sections
				if(null != contextDataNode)
					((ObjectNode) (resultEntiryNode)).set(Constants.CONTEXTDATAKEY, contextDataNode);	// Add ContextData
				entityArr.add(resultEntiryNode);
				
				for (String uuid : uuidList) {
					apiExpressionEvaluator.getExpressionEvaluatorDriver().removeCsvData(uuid);
				}

				//PE-8070 end
			}
			resultNode=mapper.createObjectNode().set("entities", entityArr);	  
		return resultNode;
	}

	public JsonNode validate(HashMap<String, JsonNode> dataMap, ArrayList<JsonNode> columnNodeList,
			JsonNode dataNode, APIExpressionEvaluator apiExpressionEvaluator, Map<String, Integer> cscDuplicateChkMap, boolean isMDARequest,
			HashMap<String, JsonNode> dimMap, List<String> metaDataColumnsOnly ) throws Exception {	//7045
		JsonNode validationNode = null;
		ObjectNode obj = null;
		String code = "";
		ArrayNode arr = null;
		JsonNode nodeDTObject = null;
		JsonNode dnCodeObject = null;
		Map<String, String> incorrectDataType = new HashMap<>();
		String result = null;
		ArrayList<ObjectNode> predefinedObjList = null;
		List<ObjectNode> rangeOrSeqObjList = null;
		ArrayList<String> dimList=null;
		//PE-5643 by default true because for true type do not fire validations.
		boolean isOptionType = true;
		JsonNode validationOBJ = null;
		JsonNode dimNode = null;
		int endPoint = Constants.ENDPOINT_DEFAULT;
		JsonNode questionType = null; //Added for 8916 Remove %
		try {
			if(isMDARequest) {
				endPoint = Constants.ENDPOINT_PERFORM_DEFAULT_ACTIONS;
			}
			
			if (columnNodeList != null && columnNodeList.size() > 0) {
				arr = mapper.createArrayNode();
				for (JsonNode node : columnNodeList) {
					code = node.get(Constants.CODE).asText();
					JsonNode currentDataMapCol = null;
					if (dataNode != null ) {
   							
						//Added for 8916 Remove %
						 questionType = node.get(Constants.QUESTION_TYPE);
					     if(!JsonUtils.isNullOrBlankOrNullNode(questionType) && 
					    		 questionType.asText().equals(Constants.PERCENTAGE) && 
					    		 null != dataMap && 
					    		 !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code)) &&  
					    		 dataNode.get(code) != null) {
					    	 String percentageValue = dataMap.get(code).asText().trim();
					    	 if(!percentageValue.isEmpty() && percentageValue.endsWith("%")){
					    		 percentageValue = percentageValue.substring(0, percentageValue.length()-1);
					    		 dataMap.put(code, mapper.readTree(percentageValue));
					    		 ((ObjectNode) dataNode).put(code, mapper.readTree(percentageValue));
					    	 }
					     }  //PE -5216 and PE-5643 Check for drop down options exist in metadata or in database.
   							if (questionType != null
   									&& ( questionType.asText().equals("dropdown") || 
   											questionType.asText().equals("radio_buttons") )) {
   								
   								// PE-8070 Clear & Prepare outputJson for dimensions
   								if(!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(code)) &&  dataNode.get(code).getNodeType()==JsonNodeType.OBJECT) {
   									Iterator<String> it = dataNode.get(code).fieldNames();
   									String dimensionKey = null;
   									while(null!=it && it.hasNext() ) {
   										dimensionKey = it.next();
   		   								isOptionType =  checkAnswerExists(node, dataNode.get(code).get(dimensionKey));
   		   								
   		   								if ( ! isOptionType ) {
   	   		   								
//												PE-8397
//   		   									// Implementation start PE : 5643
//   		   									// Clear the value anyways if checkAnswerExists==false
   		   										if(isMDARequest)
   		   										{
   		   										((ObjectNode) (dataNode.get(code))).put(dimensionKey, "");
   		   										}
//   	   		   								// Implementation end PE : 5643
   		   									
   		   									// Generate error for dimension in format : Question1.Dimension1
   		   									obj = APIExpressionEvaluator.prepareOutputJson(null, null, null, ValidationTypes.DROPDOWN,
   		   										code+"."+dimensionKey, null, null,node.has(Constants.DISPLAYLABEL) ? node.get(Constants.DISPLAYLABEL).asText() : null);
   		   								}
   		   								if (obj != null) {
   		   									arr.add(obj);
   		   									obj = null;
   		   								}
   									}

								// Clear & Prepare outputJson for non-dimensional questions (ie. simple values)
   								} else {
	   								isOptionType =  checkAnswerExists(node, dataNode.get(code));
	   								
	   								if ( ! isOptionType ) {
	   									
//										PE-8397
//	   	   								// Implementation start PE : 5643	
//	   									// Clear the value anyways if checkAnswerExists==false
									if (isMDARequest) {
										((ObjectNode) (dataNode)).put(code, "");
									}
									// Implementation end PE : 5643
	   									
	   									obj = APIExpressionEvaluator.prepareOutputJson(null, null, null, ValidationTypes.DROPDOWN,
	   											code, null, null,node.has(Constants.DISPLAYLABEL) ? node.get(Constants.DISPLAYLABEL).asText() : null);
	   								}
	   								if (obj != null) {
	   									arr.add(obj);
	   									obj = null;
	   								}
   								}
   							}
   							
   							if(null != dataMap && !JsonUtils.isNullOrBlankOrNullNode(dataMap.get(code))){
   								currentDataMapCol = dataMap.get(code);
   							}
   						//PE-9490 Round off upto 2 precision places for money question type
   							/*if(questionType != null && questionType.asText().equals(Constants.MONEY)) {
   								nodeDTObject = node.get(Constants.DATA_TYPE);	
   								if (!JsonUtils.isNullOrBlankOrNullNode(nodeDTObject)) {
   									dnCodeObject = dataNode.get(code);
   									if (!JsonUtils.isNullOrBlankOrNullNode(dnCodeObject)
   											&& !StringUtils.isEmpty(dnCodeObject.asText().trim())) {
   										incorrectDataType = new FormValidator().dataTypeCheck(dataMap, dataNode, code,
   												nodeDTObject, dnCodeObject, false, null, null);
   										boolean flag = Boolean.parseBoolean(incorrectDataType.get("dataTypeFlag"));
   										if(flag  && null != currentDataMapCol && dataNode.get(code) != null) {
   												String colStringVal = currentDataMapCol.asText().trim();
   												double colDoubleVal = currentDataMapCol.asDouble();
   												if(colStringVal.contains(".")) {
   													//RoundOff the value and return error object if it has more than 2 precision places
   													obj = DataUtility.roundOffMoneyQuestionType(dataMap, dataNode, obj, code, nodeDTObject.asText(), colStringVal, colDoubleVal);
   												}
   										}
   									}
   								}
   							}*/
   							
   							// PE 9145 : Clear code value in MDA in case of data type error
   							if(isMDARequest)
   							{
   							nodeDTObject = node.get(Constants.DATA_TYPE);
							if (!JsonUtils.isNullOrBlankOrNullNode(nodeDTObject)) {
								dnCodeObject = dataNode.get(code);
								if (isMDARequest && !JsonUtils.isNullOrBlankOrNullNode(dnCodeObject)
										&& !StringUtils.isEmpty(dnCodeObject.asText().trim())) {
									incorrectDataType = new FormValidator().dataTypeCheck(dataMap, dataNode, code,
											nodeDTObject, dnCodeObject, false, null, null);
									if (!Boolean.parseBoolean(incorrectDataType.get("dataTypeFlag"))) {
										((ObjectNode) (dataNode)).put(code, "");
									}
								}
							}
   							}
   						// PE 9145 end
						if (node.get(Constants.VALIDATIONS) != null && node.get(Constants.VALIDATIONS).size() > 0) {

							for (int i = 0; i < node.get(Constants.VALIDATIONS).size(); i++) {
								boolean rangeAlertFlag = false;
								try {
									validationNode = node.get(Constants.VALIDATIONS).get(i);
									if (validationNode.get(Constants.VALIDATION_TYPE) != null
											&& EnumUtils.isValidEnum(ValidationTypes.class,
													validationNode.get(Constants.VALIDATION_TYPE).asText().toUpperCase())) {
										switch (ValidationTypes
												.valueOf(validationNode.get(Constants.VALIDATION_TYPE).asText().toUpperCase())) {
										case REQUIRED:
											// PE - 7241
											predefinedObjList = FormValidator.validatePredefinedValidation(dataNode,node,validationNode,dataMap,null,Constants.REQUIRED);
													break;
										case PHONE:
											// PE - 7241
											predefinedObjList = FormValidator.validatePredefinedValidation(dataNode,node,validationNode,null,null,Constants.PHONE);
											break;
										case EMAIL:
											// PE - 7241
											predefinedObjList = FormValidator.validatePredefinedValidation(dataNode,node,validationNode,null,null,Constants.EMAIL);
											break;
										case RANGE:
											rangeOrSeqObjList = apiExpressionEvaluator.prepareRangeObject(dataMap, validationNode, node,dataNode);
											break;
										case RANGEVALIDATIONREFTABLE:
											rangeOrSeqObjList = apiExpressionEvaluator.prepareRefernceRangeObject(dataMap, validationNode,node);
											break;
										case EXPRESSION:
											obj = apiExpressionEvaluator.expressionEvaluator(dataMap, validationNode, node,
													dataNode, cscDuplicateChkMap,columnNodeList, metaDataColumnsOnly, endPoint);
											break;
										case ELIGIBILITY:
											arr.addAll(apiExpressionEvaluator.checkValidity(validationNode, node,
													dataNode, isMDARequest,metaDataColumnsOnly));
											break;
											
											/**
											 * @author Akhileshwar
											 * PE-7045
											 */
										case SEQUENTIALCHECK://PE-7989 metaDataColumnsOnly added in method signature
											rangeOrSeqObjList = apiExpressionEvaluator.performSequentialCheck(dataMap, validationNode, node,
													dataNode,dimMap,apiExpressionEvaluator.getColumnDataTypeMapping(), false, metaDataColumnsOnly);
											break;
										default:
										}
									} else {
										result = Constants.MANDATORY_VALIDATIONTYPE;
										obj = APIExpressionEvaluator.prepareOutputJson(null, null, result, null, null, null,
												null, null);
									}
									if (obj != null) {
										arr.add(obj);
										obj = null;
									}
									if(rangeOrSeqObjList!=null && !rangeOrSeqObjList.isEmpty())
									{
										for(int j=0;j<rangeOrSeqObjList.size();j++)
										{
											arr.add(rangeOrSeqObjList.get(j));
										}
										rangeOrSeqObjList=null;
									}
									if((null != predefinedObjList) && (predefinedObjList.size() > 0)){
										arr.addAll(predefinedObjList);
										predefinedObjList = null;
									}
									
								// PE-8353 : Log all exceptions and don't throw outside
								} catch (CustomStatusException e) {
									logger.info(JsonUtils.updateErrorNode(e, mapper.createObjectNode()).toString());
								} catch(Exception ex) {
									logger.info(ex.getMessage());
								}
								if(arr != null && arr.size() > 0 && null!=node.get(Constants.VALIDATIONS)) { 
											validationOBJ = validationNode;

											// Check if MDA action available
											if (null!=validationOBJ && null != validationOBJ.get(Constants.MDA)
													&& !validationOBJ.get(Constants.MDA).asText().equals("")
													&& !validationOBJ.get(Constants.VALIDATION_TYPE).asText().equals("expression"))
													 {
													for(JsonNode jnode : arr){
														if(Constants.ERROR.equals(jnode.get(Constants.ERROR_TYPE).asText()) && 
 																!JsonUtils.isNullOrBlankOrNullNode(jnode.get(Constants.VALIDATION_TYPE)) &&
 																validationOBJ.get(Constants.VALIDATION_TYPE).asText().equals(jnode.get(Constants.VALIDATION_TYPE).asText())){
															rangeAlertFlag = true;
															break;
														}
													}
													
												
												if(rangeAlertFlag == true){
												// MDA = exclude_row
												if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.EXECLUE_ROW)) {
													List<String> columnNodeList_Section = new ArrayList<>();
													//Getting columns in current section PE-7771
													for(int index = 0; index < columnNodeList.size(); index++){
														columnNodeList_Section.add(columnNodeList.get(index).get(Constants.CODE).asText());
													}
													if(columnNodeList_Section != null && columnNodeList_Section.contains(Constants.EXCLUDE_FLAG)){//PE-7771
														((ObjectNode) (dataNode)).put(Constants.EXCLUDE_FLAG, Constants.ANSWER_YES);
													}
												// MDA = clear_value
												} else if (validationOBJ.get(Constants.MDA).asText().equalsIgnoreCase(Constants.CLEAR_VALUE)) {
													((ObjectNode) (dataNode)).put(code, "");
												} else {//7044 condition added
													String responseData= apiExpressionEvaluator.
															expressionEvaluatorForMDA(dataMap, validationOBJ, node, cscDuplicateChkMap, metaDataColumnsOnly);
													 
													if(node.get(Constants.DIMENSIONS)!=null)
													{
														dimList=APIExpressionEvaluator.extractDimensions(node.get(Constants.DIMENSIONS));
														dimNode = mapper.createObjectNode();
														for(String dim:dimList)
														{
														((ObjectNode)dimNode).put(dim,responseData);
														}
														((ObjectNode) (dataNode)).set(code, dimNode);
													}
													
													else{
																((ObjectNode) (dataNode)).put(code, responseData);
														}
												}
											}
													 }

									}
								}
							// PE 9145 removed below unused code
							if (obj != null) {
								arr.add(obj);
								obj = null;
							}
						}

						if (node.get("questionType") != null
								&& node.get("questionType").asText().equals("checkboxes")) {

							buildCheckboxArray(dataNode,code);
						}
					}
					
				}
				if ((arr != null && arr.size() > 0)) {
					if (dataNode.get(code) != null) {
						((ObjectNode) (dataNode)).set(code, dataNode.get(code));
					}
					((ObjectNode) (dataNode)).set(Constants.VALIDATION_RESULTS, arr);
				}
				
			}
		// PE-8353 : Log all exceptions and don't throw outside
		} catch (CustomStatusException e) {
			logger.info(JsonUtils.updateErrorNode(e, mapper.createObjectNode()).toString());
		} catch(Exception ex) {
			logger.info(ex.getMessage());
		}
		return dataNode;
	}

	public void buildCheckboxArray(JsonNode dataNode,String code) 
	{
		if(!JsonUtils.isNullOrBlankOrNullNode(dataNode.get(code)))//PE-8704
	        {
	
			if( dataNode.get(code).getNodeType() != JsonNodeType.OBJECT)
			{
				ArrayNode arrNode = mapper.createArrayNode();
				String arr1[] = null;
				if( !JsonUtils.isNullNode(dataNode.get(code))) {
				switch (dataNode.get(code).getNodeType()) {
				case ARRAY:
					for (JsonNode node : dataNode.get(code)) {
						arrNode.add(node);
					}
					break;
				case NULL:
					arrNode = null;
					break;
				case STRING:
					arr1 = dataNode.get(code).asText().split(",");
					if (arr1 != null && arr1.length > 0) {
						for (int i = 0; i < arr1.length; i++) {
							arrNode.add(arr1[i]);
						}
					}
					break;
				default:
					break;
					}
				}
				((ObjectNode) (dataNode)).set(code, arrNode);
			}
			else
			{
				Iterator<Map.Entry<String, JsonNode>> it = dataNode.get(code).fields();
				Map.Entry<String, JsonNode> entry = null;
				while(it!=null && it.hasNext())
				{
					entry =it.next();
					if(entry!=null && entry.getValue().getNodeType()== JsonNodeType.ARRAY)
					{
						break;
					}
					ArrayNode arrNode = mapper.createArrayNode();
					String[] arr1 = null;
					if(entry!=null){
						arr1=entry.getValue().asText().split(",");
					}
                    if(arr1!=null && arr1.length >0)
                    {
                        for(int i=0;i<arr1.length;i++)
                        {
                            arrNode.add(arr1[i]);
                        }
                    }
                    if(null!=entry){
                        ((ObjectNode)(dataNode.get(code))).set(entry.getKey(), arrNode);
                    }
				}
				
			}
	      }//PE-8704
        else
        {
               ((ObjectNode) (dataNode)).set(code, mapper.createArrayNode());
        }

        
	}
	
	public String checkUniqueNessForEEID(String enetityJson) throws JsonParseException, JsonMappingException, IOException 
	{
		String isUnique="true";
		JsonNode entityNode=null; 
		JsonNode sectionsNode = null;	
		JsonNode dataNode=null;
		HashSet<String> eeidSet=null;
			ArrayNode entitiesNode = mapper.readValue(enetityJson, ArrayNode.class);
			for(int i=0;i<entitiesNode.size();i++) {
				entityNode = entitiesNode.get(i);
				sectionsNode = entityNode.get(Constants.SECTIONS);
				
				for (int sctnCtr=0; sctnCtr<sectionsNode.size(); sctnCtr++){
					eeidSet = new HashSet<>();
						dataNode = sectionsNode.get(sctnCtr).get(Constants.DATA);
						if(dataNode!=null)
						{
						for (int k = 0; k < dataNode.size(); k++) {
							if(dataNode.get(k).get(Constants.EMPLOYEE_EEID)!=null && !dataNode.get(k).get(Constants.EMPLOYEE_EEID).asText().equals(""))
							{
								String eeId = dataNode.get(k).get(Constants.EMPLOYEE_EEID).asText();
								if(!eeidSet.add(eeId)){
									return Constants.DUPLICATE;
								}	
							}
							else
							{
								return Constants.MISSING;
							}
						}
						}
				}
			}

		
		return isUnique;
	}
	
	/**
	 * 
	 * @param dropDownOptions
	 * @param dataNode
	 * @param code
	 * @param key
	 * @return boolean value
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static boolean checkAnswerExists(JsonNode dropDownOptions, JsonNode dataNode_code) throws SQLException, IOException  {

		ArrayNode optionsArrNode = null;
		JsonNode dbNode = null;
		Map<String, String> optionsDictionary = null;
		Map<String, String> dbData = null;
		boolean found = false;
		boolean fromDB = false;
		boolean fromJson = false;

		// PE-8269 If value is blank, return found=true. - only for /performDefaultActions
		if (JsonUtils.isNullOrBlankOrNullNode(dataNode_code) || "".equals(dataNode_code.asText()) ) {
			return true;	// Reverted temporarily due to requirement changes
		}
		dbData = new HashMap<>();
		optionsDictionary = new HashMap<>();
		if (null != dropDownOptions && null != dropDownOptions.get(Constants.OPTIONS)) {

			if (null != dropDownOptions.get(Constants.OPTIONS).get(Constants.ITEMS)) {
				optionsArrNode = (ArrayNode) dropDownOptions.get(Constants.OPTIONS).get(Constants.ITEMS);
				if (optionsArrNode.size() > 0)
					fromJson = true;
			}
			if (null != dropDownOptions.get(Constants.OPTIONS).get(Constants.REFERENCETABLE)) {
				dbNode = dropDownOptions.get(Constants.OPTIONS).get(Constants.REFERENCETABLE);
				fromDB = true;
				dbData = DBUtils.getRefTableData(dbNode.get(Constants.CODE).asText());
			}

		}
		if (fromJson) {
			for (int i = 0; i < optionsArrNode.size(); i++) {
				if (!JsonUtils.isNullOrBlankOrNullNode(optionsArrNode.get(i).get(Constants.VALUE))
						&& !JsonUtils.isNullOrBlankOrNullNode(optionsArrNode.get(i).get(Constants.DISPLAY_NAME))) {
					optionsDictionary.put(optionsArrNode.get(i).get(Constants.VALUE).asText(),
							optionsArrNode.get(i).get(Constants.DISPLAY_NAME).asText());
				}
			}

			if (optionsDictionary.containsKey(dataNode_code.asText())) {
				found = true;
			}

		} else if (fromDB && dbData.containsKey(dataNode_code.asText())) {
			found = true;
		}

		return found;
	}
	
	/**
	 * 
	 * @param dropDownOptions
	 * @param dbOutPut
	 * @return option value from Drop Down
	 * PE-9734
	 */
	
	public static String getOptionFromDropDown(JsonNode dropDownOptions, String dbOutPut) {

		logger.info("Entering checkOptionExists method {}", dbOutPut);

		ArrayNode optionsArrNode = null;
		Map<String, String> optionsDictionary = null;

		optionsDictionary = new HashMap<>();
		if (null != dropDownOptions && null != dropDownOptions.get(Constants.OPTIONS) &&
				null != dropDownOptions.get(Constants.OPTIONS).get(Constants.ITEMS)) {

			optionsArrNode = (ArrayNode) dropDownOptions.get(Constants.OPTIONS).get(Constants.ITEMS);

			for (int i = 0; i < optionsArrNode.size(); i++) {
				if (!JsonUtils.isNullOrBlankOrNullNode(optionsArrNode.get(i).get(Constants.VALUE))
						&& !JsonUtils.isNullOrBlankOrNullNode(optionsArrNode.get(i).get(Constants.DISPLAY_NAME))) {
					optionsDictionary.put(optionsArrNode.get(i).get(Constants.DISPLAY_NAME).asText(),
							optionsArrNode.get(i).get(Constants.VALUE).asText());
				}
			}
		}

		if (null != optionsDictionary.get(dbOutPut)) {
			return optionsDictionary.get(dbOutPut);
		}

		return null;

	}

}
