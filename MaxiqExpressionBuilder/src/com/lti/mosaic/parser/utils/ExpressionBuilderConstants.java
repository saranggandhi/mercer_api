package com.lti.mosaic.parser.utils;

public class ExpressionBuilderConstants {
	
	// TODO Put all Constants in .properties file
	public static final String POS_CODE_REGEX = "^[a-zA-Z]{3}\\.\\d{2}\\.\\d{3}\\.[a-zA-Z]\\d{2}$";
	public static final String COL_ANNUAL_BASE = "EMP_129";		// PE-6742
	public static final String COL_MONTH_BASE = "EMP_125";		// PE-6742
	public static final String POS_CODE = "POS_CODE";
	public static final String COL_NUM_MONTH = "EMP_128";		// PE-6742
	public static final String COL_TOTAL_GUARANTEED_CASH = "EXP_COMP2";			// PE-7406
	
	public static final String SALARY_TYPE_REGULAR = "COMP1";					// PE-7406
	public static final String SALARY_TYPE_TOTAL_GUARANTEED_CASH = "COMP2";		// PE-7406
	
	public static final String SALARY_TYPE_MSG_REGULAR = 
			"AGGREGATE_VALIDATION=msg.jobinversion.median.base.salary;"
			+ "INCUMBENT_LEVEL=msg.jobinversion.base.salary";					// PE-8877
	public static final String SALARY_TYPE_MSG_TOTAL_GUARANTEED_CASH = 
			"AGGREGATE_VALIDATION=msg.jobinversion.median.total.guarantee.cash;"
			+"INCUMBENT_LEVEL=msg.jobinversion.total.guarantee.cash";			// PE-8877
	
	public static final String LINKED = "LINKED";
	public static final String MEDIAN = "MEDIAN";
	public static final String COUNT = "COUNT";
	
	public static final String CONST_INVERSION_JSON_ARRAY = "inversionJsonArray";
	public static final String CONST_COUNTRYCODE = "countryCode";
	public static final String CONST_COMPENSATION = "__COMPENSATION";
	
	
	public static final int JOBINVERSION_DEFAULT_NUM_MONTHS = 12;
	
	public static final String GREATER_THAN = "GT"; 
	public static final String LESS_THAN = "LT"; 
	public static final String GREATER_THAN_OR_EQUALS = "GE"; 
	
	public static final String COUNTRYCODE = "countryCode";
	public static final String REGEX_FILENAME_SPLIT = "\\.(?=[^\\.]+$)";
}
